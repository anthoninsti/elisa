<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;
use Illuminate\Support\Facades\Hash;

class Clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $clientes = new  App\Models\Cliente;
        $clientes->nome = 'admin';
        $clientes->usuario = 'suporte';
        $clientes->email = 'teste@email.com';
        $clientes->senha = Hash::make('admin123');
        $clientes->save();
        echo 'inserido com sucesso';

        


    }
}
