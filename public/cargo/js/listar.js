
//Pega o valor de alteração do status
getValueStatus = function(id, st){
    return{
        id: id,
        st: st
    }
}

//Envia a seleção do cargo
sendValueCargo = function(dados) {
    $.ajax({
        type: 'POST',
        url: alter_status,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            console.log(data);
        }
    });
}

//Quando o swit de cargo é mudado
$('.cbx_cargo').change(function() {
    var dados = getValueStatus( $(this).data('id'), this.checked)
    sendValueCargo(dados)
});




