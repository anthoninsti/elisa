
var intialPeriodo = $('#periodo').val()

verifyempty = function(){
    var status = false
    
    if($('#mail').val()==""){
        $("#email").css({"visibility": "initial"});
        status = true
    }else{
        $("#email").css({"visibility": "hidden"});
    }
    if($('#periodo').val() == intialPeriodo){
        $("#eperiodo").css({"visibility": "initial"});
        status = true
    }else{
        $("#eperiodo").css({"visibility": "hidden"});
    }
    if($( "#tip-acom option:selected" ).text() == ""){
        $("#etip-acom").css({"visibility": "initial"});
        status = true
    }else{
        $("#etip-acom").css({"visibility": "hidden"});
    }
    
    if(verifyemptyline()){
        status = true
    }

    return status
}

verifyemptyline = function(){
    var status = false
    var line = $(".line");
    line.each(function(index) {
        var nome  = $(this).children().children(".nome-hosp").val() 
        var  rg   = $(this).children().children('.rg-hosp').val()
        var idade = $(this).children().children('.idade-hosp').val()

        if(nome == ""){
            $("#" + "enome" + (index+1)).css({"visibility": "initial"});
            status = true
        }else{
            $("#" + "enome" + (index+1)).css({"visibility": "hidden"});
        }
        if(rg == ""){
            $("#" + "erg" + (index+1)).css({"visibility": "initial"});
            status = true
        }else{
            $("#" + "erg" + (index+1)).css({"visibility": "hidden"});
        }

        if(idade == ""){
            $("#" + "eidade" + (index+1)).css({"visibility": "initial"});
            status = true
        }else{
            $("#" + "eidade" + (index+1)).css({"visibility": "hidden"});
        } 
        
        if(idade == ""){
            $("#" + "eidade" + (index+1)).css({"visibility": "initial"});
            status = true
        }else{
            $("#" + "eidade" + (index+1)).css({"visibility": "hidden"});
        } 
    });
    return status
}

//Pega os dados digitados nos campos
getData = function(){
    return{
        email: $('#mail').val(),
        periodo: $('#periodo').val(), 
        hospedes: getlineHosp(),
        tip_acom: $( "#tip-acom option:selected" ).text(),
        desc_pontos: $('[name="desc-pontos"]:checked').val(),
        obs: $('#obs').val(),
    }
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.store,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            console.log(data)
        }
    });
}

//Escolher a quantidade de linha para opção de qtd hospede 
lineHosp = function(qtd){
    var linhas = ''
    for (var i = 1; i <= qtd; i++) {
        linhas += `
        <div class="form-row line">
            <div class="form-group col-md-6">
                <label  class="col-form-label">Nome</label>
                <input type="text" class="form-control nome-hosp">
                <span id="`+ "enome"+ i +`" class="help-block erro"><small>informe o nome</small></span>
            </div>
            <div class="form-group col-md-3">
                <label for="inputCity" class="col-form-label">RG</label>
                <input type="text" class="form-control rg-hosp">
                <span id="`+ "erg"+ i +`" class="help-block erro"><small>informe o RG</small></span>
            </div>
            <div class="form-group col-md-3">
                <label for="inputCity" class="col-form-label">idade</label>
                <input type="text" class="form-control idade-hosp">
                <span id="`+ "eidade"+ i +`" class="help-block erro"><small>informe o nome</small></span>
            </div>
        </div>`   
    }
    $('.line-hosp').html(linhas)
}

//Retona os valores digitados nos campos
getlineHosp = function(qtd){
    var hosp = [];
    var line = $(".line");
    
    line.each(function() {
        hosp.push({
            nome:$(this).children().children(".nome-hosp").val(),
            rg:$(this).children().children('.rg-hosp').val(),
            idade:$(this).children().children('.idade-hosp').val()
        });
    });

    return hosp 
}

//Evento quando selecionando a quantidade de hospedes 
$('.qtd-pes-hosp').change(function(){
    var qtd = $(this).val() 
    lineHosp(qtd)
});

//Evento botão salvar
$('#btn-send').click(function() { 
    // console.log(verifyempty())
    var dados = getData()
    console.log(dados)   
    // sendData(dados)
    verifyempty()
});


