
var intialPeriodo = $('#dtuti').val()

verifyempty = function(){
    var status = false
    
    if($('#mail').val()==""){
        $("#email").css({"visibility": "initial"});
        status = true
    }else{
        $("#email").css({"visibility": "hidden"});
    }
    if($('#idrci').val() == ""){
        $("#eidrci").css({"visibility": "initial"});
        status = true
    }else{
        $("#eidrci").css({"visibility": "hidden"});
    }
    if($('#dtuti').val() == intialPeriodo){
        $("#edtuti").css({"visibility": "initial"});
        status = true
    }else{
        $("#edtuti").css({"visibility": "hidden"});
    }
    if($('#idempre').val() == ""){
        $("#eidempre").css({"visibility": "initial"});
        status = true
    }else{
        $("#eidempre").css({"visibility": "hidden"});
    }
    if($( "#trifinter option:selected" ).text() == ""){
        $("#etrifinter").css({"visibility": "initial"});
        status = true
    }else{
        $("#etrifinter").css({"visibility": "hidden"});
    }
    
    return status
}

//Pega os dados digitados nos campos
getData = function(){
    return{
        email: $('#mail').val(),
        idrci: $('#idrci').val(), 
        dtuti: $('#dtuti').val(), 
        idempre: $('#idempre').val(),
        trifinter: $( "#trifinter option:selected" ).text(),
        obs: $('#obs').val()
    }
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.store,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            if(data.status){
                Swal.fire({
                    icon: 'success',
                    title: 'Sucesso!',
                    text: data.msg
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                })     
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Falha!',
                    text: data.msg
                })
            }
        }
    });
}

//Evento botão salvar
$('#btn-send').click(function() { 
    if(!verifyempty()){
        var dados = getData()
        sendData(dados)
    }
});


