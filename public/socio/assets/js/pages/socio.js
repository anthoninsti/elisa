getData = function(classe){
    return{
        id:  classe.data('id')
    }
}

sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.boleto,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(output) {
            console.log(output);
        }
    });
}

$('.btn-print').click(function() {    
    var params  = getData($(this))
    sendData(params)
});