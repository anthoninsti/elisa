verifyempty = function(){
    var status = false
    
    if($('#nome').val()==""){
        $("#enome").css({"visibility": "initial"});
        status = true
    }else{
        $("#enome").css({"visibility": "hidden"});
    }
    
    if($('#cpf').val()==""){
        $("#ecpf").css({"visibility": "initial"});
        status = true
    }else{
        $("#ecpf").css({"visibility": "hidden"});
    }

    if($('#cpf').val()==""){
        $("#ecpf").css({"visibility": "initial"});
        status = true
    }else{
        $("#ecpf").css({"visibility": "hidden"});
    }
    if($('#email').val()==""){
        $("#eemail").css({"visibility": "initial"});
        status = true
    }else{
        $("#eemail").css({"visibility": "hidden"});
    }

    return status
}   

//Pega os dados digitados nos campos
getData = function(){
    return{
        nome:$('#nome').val(),
        apldo:$('#apldo').val(),
        email:$('#email').val(),
        n_ident:$('#n_ident').val(),
        cpf:$('#cpf').val(),
        sexo:$('#sexo option:selected').val(),
        id_cargo:$('#cargo option:selected').data('id')
    }
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.store,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            if(data.status){
                Swal.fire({
                    icon: 'success',
                    title: 'Sucesso!',
                    text: data.msg
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                })     
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Falha!',
                    text: data.msg
                })
            }
        }
    });
}

//Evento botão salvar
$('#btn-send').click(function() { 
    console.log(getData())
    if(!verifyempty()){
        var dados = getData()
        sendData(dados)
    }
});


