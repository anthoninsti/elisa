var possui = 1;

verifyempty = function(){
    var status = false
    
    if($('#nome').val()==""){
        $("#enome").css({"visibility": "initial"});
        status = true
    }else{
        $("#enome").css({"visibility": "hidden"});
    }

    if($('#dt_nsc').val()==""){
        $("#edt_nsc").css({"visibility": "initial"});
        status = true
    }else{
        $("#edt_nsc").css({"visibility": "hidden"});
    }

    if($('#cpf').val()==""){
        $("#ecpf").css({"visibility": "initial"});
        status = true
    }else{
        $("#ecpf").css({"visibility": "hidden"});
    }

    if($('#rua').val()==""){
        $("#erua").css({"visibility": "initial"});
        status = true
    }else{
        $("#erua").css({"visibility": "hidden"});
    }

    if(possui == 1 ){
        
        if($('#nome2').val()==""){
            $("#enome2").css({"visibility": "initial"});
            status = true
        }else{
            $("#enome2").css({"visibility": "hidden"});
        }

        if($('#num').val()==""){
            $("#enum").css({"visibility": "initial"});
            status = true
        }else{
            $("#enum").css({"visibility": "hidden"});
        }   
    }

    if($('#bairro').val()==""){
        $("#ebairro").css({"visibility": "initial"});
        status = true
    }else{
        $("#ebairro").css({"visibility": "hidden"});
    }

    if($('#cidade option:selected').text()=="Selecione a opção"){
        $("#cidade").css({"visibility": "initial"});
        status = true
    }else{
        $("#cidade").css({"visibility": "hidden"});
    }

    return status
}

//Evento quando selecionando a quantidade de hospedes 
$('.possui-companheirx').change(function(){
    var st = $(this).val() 
    if(st == 1){
        possui = 1;
        $('#line-partner').css({"visibility": "initial"});
    }else{
        possui = 0;
        $('#line-partner').css({"visibility": "hidden"});
    }   
});


//Pega os dados digitados nos campos
getData = function(){
    return{
        nome:$('#nome').val(),
        apldo:$('#apldo').val(),
        dt_nsc:$('#dt_nsc').val(),
        n_ident:$('#n_ident').val(),
        cpf:$('#cpf').val(),
        num_card_sus:$('#num_card_sus').val(),
        possui:possui,
        nome2:$('#nome2').val(),
        apldo2:$('#apldo2').val(),
        dt_nsc2:$('#dt_nsc2').val(),
        tel:$('#tel').val(),   
        cel:$('#cel').val(), 
        email:$('#email').val(),
        rua:$('#rua').val(),   
        numero:$('#num').val(),
        complemento:$('#comple').val(),
        id_cid:$('#cidade option:selected').data("id"),
        bairro:$('#bairro').val(),
        cep:$('#cep').val(),
        obs:$('#obs').val()
    }
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.store,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            console.log(data)
            if(data.status){
                Swal.fire({
                    icon: 'success',
                    title: 'Sucesso!',
                    text: data.msg
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                })     
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Falha!',
                    text: data.msg
                })
            }
        }
    });
}

//Evento botão salvar
$('#btn-send').click(function() { 
    if(!verifyempty()){
        var dados = getData()
        sendData(dados)
    }
});


