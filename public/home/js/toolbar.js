//Escolher a quantidade de linha para opção de qtd hospede 
lineHosp = function(qtd){
    var linhas = ''
    for (var i = 1; i <= qtd; i++) {
        linhas += `
        <a href="javascript:void(0);" class="dropdown-item notify-item">
            <div class="media">
                <img class="d-flex mr-2 rounded-circle" src="{{asset('socio/assets/images/users/def_user.png')}}" alt="Generic placeholder image" height="32">
                <div class="media-body">
                    <h5 class="m-0 font-14">Erwin Brown</h5>
                    <span class="font-12 mb-0">UI Designer</span>
                </div>
            </div>
        </a>`   
    }
    $('.line-hosp').html(linhas)
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    var url = "paciente/find"
    $.ajax({
        type: 'POST',
        url: base_url + url,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            var result = data.pacientes;
            var url = 'paciente/page/'
        
            var linhas =''
            console.log(result)
            for (var i in result) {
                var id   = result[i].id; 
                var nome = result[i].nome; 
                linhas += `
                <a href="`+ base_url + url + id +`" class="dropdown-item notify-item">
                    <div class="media">
                        
                        <div class="media-body">
                            <h5 class="m-0 font-14">`+id+`</h5>
                            <span class="font-12 mb-0">`+nome+`</span>
                        </div>
                    </div>
                </a>`  
            }
            $('.cn').html(linhas)   
        }
    });
}

//Pega os dados digitados nos campos
getData = function(paramn){
    return{
        paramn: paramn
    }
}

$('#btn-save').on('click', function(){
    sendData(getData());
});

$("#top-search").keyup(function() { 
    sendData(getData($(this).val()));
});


