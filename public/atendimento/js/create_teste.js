//Retona os valores digitados nos campos
getline = function(qtd){
    var testes = [];
    var line = $(".line");
    
    line.each(function() {
        testes.push({
            id:$(this).children(".custom-control-input").data("id"),
            checked: $(this).children(".custom-control-input").is(":checked")
        });
    });

    return testes 
}

//Pega os dados digitados nos campos
getData = function(){
    return{
        id_atd: $("#atd").data("id_atd"),
        obs: $('#obs').val(),
        testes: getline(), 
    }
}

//Envia os dados da reserva para o controller
sendData = function(dados) {
    $.ajax({
        type: 'POST',
        url: config.routes.store,
        data: dados,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(data) {
            console.log(data);
            if(data.status){
                Swal.fire({
                    icon: 'success',
                    title: 'Sucesso!',
                    text: data.msg
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                })     
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Falha!',
                    text: data.msg
                })
            }
        }
    });
}

$('#btn-save').on('click', function(){
    sendData(getData());
});
