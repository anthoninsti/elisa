<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Classifica extends Model
{
    public $timestamps;

    /*Retorna classificação de acordo com a classe
    * create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findByClasse($atd_id_test, $classe)
    {
        return DB::select("SELECT 
        cla.id,
        cla.nome,
        cla.classe,
        cla.descicao,
        cla.st,
        IF((SELECT  
        COUNT(tc.id)
        FROM test_classificas tc
        INNER JOIN atd_tests ats ON tc.id_atd_test = ats.id
        INNER JOIN classificas c ON tc.id_classifica = c.id
        WHERE ats.id = ".$atd_id_test." AND c.id = cla.id) > 0, TRUE, FALSE) as tem_test
        FROM classificas cla 
        WHERE cla.classe = '".$classe."'");     
    
    }

    /*Retorna os valores para verificar se tem classificação ou não
    * create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function temClassificacao($atd_id_test)
    {
        $res = DB::select("SELECT 
        IF(SUM(IF(c.classe = 1, 1, 0)) > 0, TRUE, FALSE) as cls1,
        IF(SUM(IF(c.classe = 2, 1, 0)) > 0, TRUE, FALSE) as cls2
        
        FROM test_classificas tc
        INNER JOIN atd_tests ats ON tc.id_atd_test = ats.id
        INNER JOIN classificas c ON tc.id_classifica = c.id
        WHERE ats.id = ".$atd_id_test."");     

        if($res != null){
            return $res[0];
        }
    
        return $res;
    }
}
