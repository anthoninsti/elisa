<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AtdTest extends Model
{
    public $timestamps;

    /* Cria todos os testes de um atendimento
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function createAllTestsAtd($id_atd, $testes)
    {
        foreach($testes as $teste){
            if($teste["checked"] == "true"){
                $atdTest = new AtdTest();
                $atdTest->id_test = $teste["id"];
                $atdTest->id_atd = $id_atd;
                $atdTest->st = 1;
                $atdTest->save();
            }
        }
    }

    /* Deleta todos os testes de um atendimento
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function delAllTestsAtd($id_atd){
        return self::where('id_atd', $id_atd)->delete();
    }

    /* Retorna uma lista de teste indicando se tem atendimento
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAllTestsAtd($id_atd)
    {
        $res = DB::select("SELECT t.id, t.nome, 
        IF((SELECT 
           COUNT(id)
           FROM atd_tests 
           WHERE id_test = t.id AND id_atd = '".$id_atd."'
          ) > 0, 'checked','') as tem_atd 
        FROM tests t");     

        return $res;
    }

    /* Atualiza as informações do status de teste de atendiemnto
    *  create by Adão - jose.carlos.adao@hotmail.com 12.04.2020*/
    public static function updateResultTest($id_atd_test, $result)
    {
       return self::where('id', $id_atd_test)->update(['res' => $result]);   
    }
}
