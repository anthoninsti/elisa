<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cliente extends Model
{
    //
    public $timestamps;
    
 
    /** 
    * Retorna o cliente de acordo com cpf ou o e-mail
    * Return the costomer according to cpf or e-mail
    * create by Adão - jose.carlos.adao@hotmail.com 26.02.2020*/
    public static function findClienteByCpfOrEmail($param)
    {
        $res = DB::select("SELECT * 
        FROM `clientes` 
        WHERE REPLACE(REPLACE(cpf_1, '.', ''), '-', '') = '".$param."' 
        OR email_1 = '".$param."' 
        OR email_2 = '".$param."'
        OR cpf_1 = '".$param."'
        LIMIT 1");   
        
        if($res != null){
            return $res[0];
        }

        return $res;
    }

    /** 
    * Metódo usado para retornar os telefones válidos dos clientes
    * Method used to return valid customers' phones
    * create by Adão - jose.carlos.adao@hotmail.com 27.02.2020*/
    public static function getValidPhones($cliente)
    {   
        $tels = [];
        if($cliente->tel_fixo_1 != "" && $cliente->tel_fixo_1 != "( ) -"){
            array_push($tels, $cliente->tel_fixo_1);
        }
        if($cliente->tel_fixo_2 != "" && $cliente->tel_fixo_2 != "( ) -"){
            array_push($tels, $cliente->tel_fixo_2 );
        }
        if($cliente->celular_1 != "" && $cliente->celular_1 != "( ) -"){
            array_push($tels, $cliente->celular_1 );
        }
        if($cliente->celular_2 != "" && $cliente->celular_2 != "( ) -"){
            array_push($tels, $cliente->celular_2 );
        }

        return $tels;
    }

    /** 
    * Metódo usado para retornar os emails válidos dos clientes
    * Method used to return valid customers' emails
    * create by Adão - jose.carlos.adao@hotmail.com 27.02.2020*/
    public static function getValidMails($cliente)
    {   
        $emails = [];
        
        if($cliente->email_1 != ""){
            array_push($emails, $cliente->email_1);
        }
        if($cliente->email_2 != ""){
            array_push($emails, $cliente->email_2);
        }

        return $emails;
    }

    /** 
    * Metódo usado para retornar o endereço do usuário
    * Method used to return the user's address
    * create by Adão - jose.carlos.adao@hotmail.com 27.02.2020*/
    public static function getValidAddress($cliente)
    {   
        $address = "";

        if($cliente->rua != ""){
            $address .= $cliente->rua;
        }
        if($cliente->numero != ""){
            $address .= ", nº ".$cliente->numero;
        }
        if($cliente->complemento != ""){
            $address .= " ".$cliente->complemento;
        }
        if($cliente->cep != ""){
            $address .= ", cep: ".$cliente->cep;
        }
        if($cliente->bairro != ""){
            $address .= ", ".$cliente->bairro;
        }
        if($cliente->cidade != ""){
            $address .= " - ".$cliente->cidade;
        } 
        if($cliente->estado != ""){
            $address .= " - ".$cliente->estado;
        }

        return $address;
    }

    /** 
    * Metódo usado para retornar o contrato do usuário
    * Method used to return the user contract
    * create by Adão - jose.carlos.adao@hotmail.com 27.02.2020*/
    public static function getInfoContract($cliente)
    {   
        $res = DB::select("SELECT 
        num_contrato, 
        CAST(REPLACE(REPLACE(replace(replace(replace(replace(replace(replace(quant_pontos,'.1', ''), '.2', ''), '.3',''), '.4',''), '.5', ''), '.6', ''), '.', ''), ',' , '.') as DECIMAL(12,2)) AS quant_pontos,
        duracao_contrato,
        (uso_inicial + replace(replace(duracao_contrato, 'Anos', ''),'anos', '')) as usavel_ate,
        CAST(REPLACE(REPLACE(val_total, '.', ''), ',' , '.') as DECIMAL(12,2)) as val_total

        FROM clientes cli INNER JOIN contratos ctr
        ON cli.contrato_id = ctr.id
        WHERE cli.id = ".$cliente->id."
        LIMIT 1");   
        
        if($res != null){
            return $res[0];
        }

        return $res;
    }
}
