<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TratamentoMedicamento extends Model
{
    public $timestamps;
    
    /******************************************************************
    *                              SELECTS                            *
    ******************************************************************/
    /* Retorna os medicamentos dos tratamentos de acordo o id do tratamento
    *  create by Adão - jose.carlos.adao@hotmail.com 17.04.2021*/
    public static function findByIdTCls($idtt, $classe)
    {
        return DB::select("SELECT 
        id,
        dt,
        st

        FROM tratamento_medicamentos 
        WHERE id_test_tratamentos = ".$idtt." AND classe = '".$classe."'");     
    }
    /**********************************************************
    *                           UPDATE 
    **********************************************************/
    public static function updateAllTratMed($trats)
    {
        foreach ($trats as $trat) {
            TratamentoMedicamento::where('id', $trat['idt'])
            ->update([
                'dt' => $trat['dt'],
                'st' => $trat['st']
            ]);
        }
    }

}
