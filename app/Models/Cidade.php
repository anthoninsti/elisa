<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cidade extends Model
{
    public $timestamps;

    /* Retorna todas as cidades
    * create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findAllCid()
    {
        $res = DB::select("SELECT * 
        FROM cidades");     
        return $res;
    }
}
