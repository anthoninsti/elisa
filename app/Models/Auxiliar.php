<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{
    /*******************************************************
    *                         AUXILIAR                     * 
    *******************************************************/
    /*Função usada para remover caracter especiais
    * Create by Adão - jose.carlos.adao@hotmail.com 12.08.2020*/
    public function remCtr($palavra)
    {   
        return strtolower(preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç|Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), str_replace(" ", "", $palavra)));
    }

    /*Função usada para converter valor decimal em monetary
	create by Adão 19-12-2019*/
	public static function decToMoney($valor) { 
		return 'R$ '.number_format($valor, 2, ',', '.'); 
	}

    /*Função usada para formatar a forma de pagamento 
	create by Adão 01-03-2021*/
	public static function formatFormaPagamento($forma) { 
		switch ($forma) {
            case "autorizacao_de_credito":
               return "Autorização de crédito";
            case "boleto_bancario":
                return "Boleto bancário";
            case "carne":
                return "Carnê";
            case "transferencia_bancaria":
                return "Transferência bancária";
            case "cheque":
                return "Cheque";
            case "transferencia_offline":
                return "Transferência offline";
            case "deposito_bancario":
                return "Depósito bancário";
            case "cartao_de_credito":
                return "Cartão de crédito";
            default:
                return "";
        }
	}

    /*Função usada para formatar a escolha de opção de sexo  
	create by Adão 04-04-2021*/
	public static function formatSexoString($sexo) { 
		switch ($sexo) {
            case "Masculino":
               return "M";
            case "Feminino":
                return "F";
            case "Indefinido":
                return "I";
            case "M":
                return "Masculino";
            case "F":
                return "Feminino";
            case "I":
                return "Indefinido";
            default:
                return "";
        }
	}

    
}
