<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Point extends Model
{
    public $timestamps;

    /* 
    * Retorna todos os crétidos pontos de um contrato
    * Return all credit points of a contract
    * create by Adão - jose.carlos.adao@hotmail.com 01.03.2020*/
    public static function creditoPtCtr ($ctr)
    {
        return DB::select("SELECT
        CONCAT_WS('/', SUBSTR(REPLACE(data,'-',''),7,2), 
        SUBSTR(REPLACE(data,'-',''),5,2),
        SUBSTR(REPLACE(data,'-',''),1,4)) AS data,
        points, 
        full_name 

        FROM points p  INNER JOIN users u
        ON p.cadastrado_por = u.id 
        WHERE p.num_contrato = '".$ctr."'");   
    }

    /* 
    * Retorna os informações créditos de pontos de um determinando contrato
    * Return the information point credits for a given contract
    * create by Adão - jose.carlos.adao@hotmail.com 01.03.2020*/
    public static function infoCredPoint($ctr)
    {
        return DB::select("SELECT
        SUM(points) AS soma
        FROM points
        WHERE num_contrato = '".$ctr."'")[0];   
    }

    /* 
    * Calcula os pontos disponíveis do contrato
    * Calculate the available points of the contract
    * create by Adão - jose.carlos.adao@hotmail.com 01.03.2020*/
    public static function calDispoPoints($vctr, $totp, $utip, $credp, $vpago)
    {   
        $tot = round(($vpago * $totp / $vctr) + $credp);
        return "" . ($tot - $utip);
    }

    /* 
    * Calcula os pontos utilizados de um contrato
    * Calculates the points used in the contract
    * create by Adão - jose.carlos.adao@hotmail.com 01.03.2020*/
    public static function calUsedPoints($ctr)
    {
        return DB::select("SELECT
        CONCAT_WS('/', SUBSTR(REPLACE(data,'-',''),7,2), 
        SUBSTR(REPLACE(data,'-',''),5,2),
        SUBSTR(REPLACE(data,'-',''),1,4)) AS data,
        points, 
        full_name 

        FROM points p  INNER JOIN users u
        ON p.cadastrado_por = u.id 
        WHERE p.num_contrato = '".$ctr."'");   
    }
}
