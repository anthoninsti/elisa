<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    public $timestamps;

    /* Retorna uma lista com todos funcionários
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2021*/
    public static function getAllTests()
    {
        $res = DB::select("SELECT * 
        FROM tests");     

        return $res;
    }

    /* Retorna todos os testes de um aendimento     
    *  create by Adão - jose.carlos.adao@hotmail.com 11.04.2021*/
    public static function getAllTestsAtd($idatd)
    {
        $res = DB::select("SELECT
        p.id as id_pac,
        atd.id as id_atd,
        atd.data as dt_atd,
        atds.id as id_atd_test, 
        t.nome as nome_test,
        atds.res

        FROM tests t
        INNER JOIN atd_tests atds ON t.id = atds.id_test
        INNER JOIN atendimentos atd ON atd.id = atds.id_atd
        INNER JOIN pacientes p ON p.id = atd.id_parc
        WHERE atds.id_atd = '".$idatd."'");     

        return $res;
    }

    /* Retorna todos os testes de um aendimento     
    *  create by Adão - jose.carlos.adao@hotmail.com 11.04.2021*/
    public static function getTestsAtdById($idtest)
    {
        $res = DB::select("SELECT
        t.nome as nome_test,
        atds.id as id_atds, 
        atds.res

        FROM tests t
        INNER JOIN atd_tests atds ON t.id = atds.id_test
        INNER JOIN atendimentos atd ON atd.id = atds.id_atd
        WHERE atds.id = '".$idtest."'");     
        if($res != null){
            return $res[0];
        }
        return $res;
    }

   
   
}
