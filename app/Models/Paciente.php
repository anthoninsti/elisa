<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Paciente extends Model
{
    public $timestamps;

    /* Retorna as informações do paciente com base na chave de e-mail
    *  create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findPacienteByEmail($param)
    {
        $res = DB::select("SELECT id, nome, email 
        FROM pacientes 
        WHERE email = '".$param."' 
        LIMIT 1");     
        
        if($res != null){
            return $res[0];
        }

        return $res;
    }

    /* Retorna as informações do paciente com base na chave de id
    *  create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findPacienteById($param)
    {
        $res = DB::select("SELECT id, nome, email, id_companheiro 
        FROM pacientes 
        WHERE id = '".$param."' 
        LIMIT 1");     
        
        if($res != null){
            return $res[0];
        }

        return $res;
    }

    /* Retorna todas as informações do paciente com base na chave de id
    *  create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findAllInfoById($id_pac)
    {
        $res = DB::select("SELECT 
        p.id as id_pac, 
        p.nome as nome_pac,
        p.apldo as apldo_pac,
        p.dt_nsc as dt_nsc_pac, 
        p.n_ident as n_ident_pac, 
        p.cpf as cpf_pac, 
        p.num_card_sus as num_cad_sus_pac, 
        p.tel,
        p.email,
        p.sexo as sexo_pac, 
        
        c.id as id_comp,
        c.nome as nome_comp,
        c.apldo as apldo_comp,
        c.dt_nsc as dt_nsc_comp,
        c.cpf as cpf_comp,
        c.num_card_sus as num_cad_sus_comp,
        c.sexo as sexo_comp,
        
        e.id as id_end,
        e.rua, 
        e.num, 
        e.comple, 
        e.bairro,
        e.cep,
        
        ci.id as id_cid, 
        ci.nome as nome_cid, 
        ci.estado
        
        FROM pacientes p
        LEFT JOIN companheirxes c 
        ON p.id_companheiro = c.id
        LEFT JOIN enderecos e
        ON p.id_endrc = e.id
        LEFT JOIN cidades ci
        ON e.id_cid = ci.id
        WHERE p.id = ".$id_pac."");     
        
        if($res != null){
            return $res[0];
        }

        return $res;
    }

    /* Retorna os tratamentos do paciente que estão em execusão
    *  create by Adão - jose.carlos.adao@hotmail.com 03.05.2020*/
    public static function seeksTreatments($id_atd)
    {
        return DB::select(" SELECT
        a.id,
        t.tipo,
        t.procedimento,
        tm.dt,
        tm.st
        
        FROM tratamento_medicamentos tm 
        INNER JOIN test_tratamentos tt 
        ON tm.id_test_tratamentos = tt.id
        INNER JOIN tratamentos t
        ON tt.id_tratamento = t.id
        INNER JOIN atd_tests ats
        ON tt.id_atd_test = ats.id
        INNER JOIN atendimentos a
        ON ats.id_atd = a.id
        WHERE a.id = '".$id_atd."'");

    }
    
    /* Retorna uma lista com todos os pacientes
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAllPacientes()
    {
        $res = DB::select("SELECT 
        p.id as id_pac, 
        p.nome as nome_pac, 
        p.apldo, 
        p.dt_nsc, 
        p.email, 
        cel,
        c.id as id_comp,
        c.nome as nome_comp  
        FROM pacientes p 
        LEFT JOIN companheirxes c 
        ON p.id_companheiro = c.id");     

        return $res;
    }

    /* Retorna uma lista com todos os pacientes
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getPacientesLike($param)
    {
        return DB::select("SELECT
        a.id,
        p.nome
        
        FROM pacientes p 
        INNER JOIN atendimentos a 
        ON a.id_parc = p.id
        WHERE P.nome LIKE '%".$param."%' OR a.id LIKE '%".$param."%'");     
        
    }
}
