<?php
/*================================================================================
    Project: Página dos Sócios - VIPCLUBSI  
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
===================================================================================*/ 
namespace App\Models\Home\Dev;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Socio extends Model
{   

   
    /*Retorna todos socios cadastrados para obter recursos de imagens 
    *Return register all members to obtain image resources  
    *create by Adão - jose.carlos.adao@hotmail.com 27.08.2020*/
    public static function getSocios()
    {
        return DB::table('home_socios')->get();
    }
}
