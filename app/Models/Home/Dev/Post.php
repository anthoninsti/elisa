<?php

namespace App\Models\Home\Dev;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /*Gera um nome para fotografia 
    *Create by Adão 25.08.2020*/
    public static function createNomePhoto(){
        return "post".rand(1000, 9999).rand(1, 9);
    }
}
