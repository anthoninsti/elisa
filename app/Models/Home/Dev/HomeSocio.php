<?php
/*================================================================================
    Project: Página dos Sócios - VIPCLUBSI  
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
===================================================================================*/ 

namespace App\Models\Home\Dev;

use Illuminate\Database\Eloquent\Model;

class HomeSocio extends Model
{
    /*******************************************************
    *                         AUXILIAR                     * 
    *******************************************************/
    /*Função usada para remover caracter especiais
    * Create by Adão - jose.carlos.adao@hotmail.com 12.08.2020*/
    public static function remCtr($palavra)
    {   
        return strtolower(preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç|Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), str_replace(" ", "", $palavra)));
    }
}
