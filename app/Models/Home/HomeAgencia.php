<?php

namespace App\models\Home;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomeAgencia extends Model
{
    /*Retorna uma agência de acordo com o id
    *create by Adão - jose.carlos.adao@hotmail.com 18.08.2020*/
    public static function getAgnById($id)
    {
        $agn = DB::select("SELECT     
        id,
        url_img,
        description,
        peso

        FROM home_agencias
        WHERE id = '$id'");   
        if($agn != null)
            return$agn[0];
        
        return null;
    }
    
    /**********************************************************/
    /*                          UPDATE                        */
    /**********************************************************/
    /*Atualiza as informações da agencia
    create by Adão jose.carlos.adao@hotamil.com - 18.08.2020*/
    public static function updateAgenciaById($id, $url_img, $description, $peso)
    {   
        return DB::statement("UPDATE 
        home_agencias SET
        description  = '$description', 
        url_img = '$url_img',
        peso = '$peso'
        WHERE id = '$id'");

    }
}
