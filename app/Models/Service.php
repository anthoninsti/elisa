<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
     /* 
    * Envia mesagem push notification 
    * create by Adão - jose.carlos.adao@hotmail.com 11.05.2021*/
    public static function sendResultTestFCM($title, $msg, $receiver)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $config = Configura::find(1);

        $fields = array(
            'to' => $receiver,
            'notification' => array(
                'title'=>$title, 
                'body'=> $msg),
            'data' => array(
                'title' => $title,
                'content' => $msg
            )
        );

        $fields = json_encode($fields);
        $headers = array(
            'Authorization: key=' . env('IDFCM'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);
       
        
        var_dump($result);
        curl_close($ch);
    }
}
