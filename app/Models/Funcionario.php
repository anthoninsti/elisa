<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\Funcionario as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Funcionario extends Authenticatable implements JWTSubject
{
    public $timestamps;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /* Retorna uma lista com todos funcionários
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAllFuncionarios()
    {
        $res = DB::select("SELECT 
        f.id as id_func, 
        f.nome as nome_func, 
        username,
        email,
        c.nome as nome_carg
        FROM funcionarios f 
        LEFT JOIN cargos c 
        ON f.id_cargo = c.id");     

        return $res;
    }

    /* Retorna o funcionario com seu respectivo cargo
    *  create by Adão - jose.carlos.adao@hotmail.com 22.04.2020*/
    public static function getFuncWithCargo($id)
    {   
        
        $res = DB::select("SELECT 
        f.id as id_func, 
        f.nome as nome_func, 
        username,
        email,
        n_ident,
        cpf,
        sexo, 
        c.id as id_cargo,     
        c.nome as nome_carg
        FROM funcionarios f 
        LEFT JOIN cargos c 
        ON f.id_cargo = c.id
        WHERE f.id  = ".$id."");  
        if($res != null)
            return $res[0];   

        return $res;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
}
