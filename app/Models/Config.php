<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Config extends Model
{
    /*Retorna a configuração de acordo com o id
    *create by Adão - jose.carlos.adao@hotmail.com 13/08/2020*/
    public static function getConfigById($id)
    {
        return DB::select("SELECT     
        id,
        url_banner
        FROM configs
        WHERE id = '$id'")[0];   
    }

}
