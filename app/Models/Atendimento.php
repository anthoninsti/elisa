<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Atendimento extends Model
{
    public $timestamps;

    /* Retorna uma lista com todos os atendimentos
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAllAtendimentos($date = "")
    {
        $res = DB::select("SELECT 
        a.id as id_atd,
        p.id as id_pac, 
        p.nome as nome_pac, 
        p.apldo, 
        p.dt_nsc, 
        p.email, 
        cel,
        c.id as id_comp,
        c.nome as nome_comp,

        (SELECT 
		CONCAT(
			'[', 
			GROUP_CONCAT(JSON_OBJECT('id', ts.id, 'nome', t.nome, 'res', ts.res)),
			']'
		) as t        	
		FROM atd_tests ts
        INNER JOIN tests t
        ON ts.id_test = t.id
        WHERE id_atd = a.id) AS testes
       
        FROM pacientes p 
        LEFT JOIN companheirxes c 
        ON p.id_companheiro = c.id
        INNER JOIN atendimentos a 
        ON a.id_parc = p.id
        ORDER BY p.id asc"); 
        
        return $res;
    }

    /* Retorna todoas atendimento de acordo com a data
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAtdimentosByDate($date)
    {
        $res = DB::select("SELECT 
        a.id as id_atd,
        p.id as id_pac, 
        p.nome as nome_pac, 
        p.apldo, 
        p.dt_nsc, 
        p.email, 
        cel,
        c.id as id_comp,
        c.nome as nome_comp,

        (SELECT 
		CONCAT(
			'[', 
			GROUP_CONCAT(JSON_OBJECT('id', ts.id, 'nome', t.nome, 'res', ts.res)),
			']'
		) as t        	
		FROM atd_tests ts
        INNER JOIN tests t
        ON ts.id_test = t.id
        WHERE id_atd = a.id) AS testes
       
        FROM pacientes p 
        LEFT JOIN companheirxes c 
        ON p.id_companheiro = c.id
        INNER JOIN atendimentos a 
        ON a.id_parc = p.id
        WHERE a.data LIKE '%".$date."%'
        ORDER BY p.id asc"); 
        
        return $res;
    }

    /* Retorna o atendimento com paciente e companheiro
    *  create by Adão - jose.carlos.adao@hotmail.com 09.04.2021*/
    public static function getAtdWithPac($id_atd)
    {
        $res = DB::select("SELECT 
        a.id as id_atd,

        CONCAT_WS('/', SUBSTR(REPLACE(a.data,'-',''),7,2), 
        SUBSTR(REPLACE(a.data,'-',''),5,2),
        SUBSTR(REPLACE(a.data,'-',''),1,4)) AS data,

        CASE a.st 
        WHEN 1 THEN 'Testes Solicitado' 
        WHEN 2 THEN 'Tratamento Iniciado'
        ELSE 'Tratamento Finalizado' END AS st,

        p.id as id_pac, 
        p.nome as nome_pac, 
        p.apldo,
        p.cpf, 
        p.n_ident, 
        p.num_card_sus,

        p.dt_nsc,
        p.tel,  
        p.email, 
        cel,

        c.id as id_comp,
        c.nome as nome_comp,
        c.apldo as capldo,
        c.cpf as ccpf,
        c.dt_nsc as cdt_nsc, 
        c.num_card_sus as cnum_card_sus,

        e.rua, 
        e.num, 
        e.comple, 
        e.bairro, 
        e.cep,
        cd.nome as cid_nome,
        cd.estado

        FROM pacientes p 
        LEFT JOIN companheirxes c 
        ON p.id_companheiro = c.id
        INNER JOIN atendimentos a 
        ON a.id_parc = p.id
        LEFT JOIN enderecos e 
        ON p.id_endrc = e.id
        LEFT JOIN cidades cd 
        ON e.id_cid = cd.id
        WHERE a.id = '".$id_atd."'"); 

        if($res != null){
            return $res[0];
        }
        
        return $res;
    }

    /* Retorna os testes de uma atendimento
    *  create by Adão - jose.carlos.adao@hotmail.com 04.04.2020*/
    public static function getAllTestsAtd($id_atd)
    {
        $res = DB::select("SELECT 
        getTestes('".$id_atd."') as testes");
        if($res != null){
         return (array)json_decode($res[0]->testes);
        }
        
        return $res;
    }
}
