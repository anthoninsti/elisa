<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companheirx extends Model
{
    public $timestamps;
    
    protected $fillable = [
        'nome',
        'apldo', 
        'dt_nsc',
        'cpf',
        'num_card_sus',
        'sexo'
    ];
}
