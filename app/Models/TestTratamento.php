<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TestTratamento extends Model
{
    public $timestamps;

    /******************************************************************
    *                              SELECTS                            *
    ******************************************************************/

    /* Retorna a classe de uma classicicação de acordo o id test classificação 
    *  create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findClassByIdTCls($param)
    {
        $res = DB::select("SELECT c.classe
        FROM test_classificas tc
        INNER JOIN classificas c 
        ON c.id = tc.id_classifica
        WHERE tc.id_classifica = ".$param."");     
        if($res != null){
            return $res[0];
        }

        return $res;
    }

    /* Retorna o teste tratamento de acordo com id atd test 
    *  create by Adão - jose.carlos.adao@hotmail.com 21.03.2020*/
    public static function findByIdAtdTest($param, $classe)
    {
        $res = DB::select("SELECT   
        tm.id as id,
        tm.dt,
        tm.st
        FROM test_tratamentos tt
        INNER JOIN tratamento_medicamentos tm
        ON tm.id_test_tratamentos = tt.id
        WHERE tt.id_atd_test = ".$param." AND tm.classe = '".$classe."'");     
        
        return $res;
    }

}
