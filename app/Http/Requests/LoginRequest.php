<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'usuario' => ['required'],
            'senha' =>['required','min:6','max:20']
        ];
    }

    public function messages(){
        return [
            'usuario.requerid' => 'O usuário é de preenchimento obrigatório',
            'usuario.text' => 'Usuário tem que ser válido',
            'senha.requerid' => 'Senha é obrigatória',
            'senha.min' => 'A senha tem que ter no minimo :size caracteres.',
            'senha.max' => 'A senha tem que ter no máximo :size caracteres.'
        ];
    }
}
