<?php

namespace App\Http\Controllers;

use App\Models\Classifica;
use App\Models\TestClassifica;
use App\Models\TestTratamento;
use App\Models\TratamentoMedicamento;
use Illuminate\Http\Request;

class ClassificaController extends Controller
{
    /**
     * Cria o destroy uma classificacao 
     * Create by Adão 12-04-2021
     * @return \Illuminate\Http\Response
     */
    public function createOrdestroy(Request $request)
    {   
        $r      = $request->r;
        $idat   = $request->idat;
        $idc    = $request->idc;
        $classe = $request->classe;
        
        if($r == "true"){
            $tc = new TestClassifica();
            $tc->id_classifica = $idc;
            $tc->id_atd_test = $idat;
            if($tc->save()){

                $tt = TestTratamento::findClassByIdTCls($tc->id_classifica);
                //Cria o test tratamento
                $testTratamento  = new TestTratamento();
                $testTratamento->id_tratamento = $tt->classe;
                $testTratamento->id_atd_test   = $idat;
                $testTratamento->id_classifica = $idc;
                $testTratamento->parceiro_tratado = 0;
                $testTratamento->st = "1";
                
                if($testTratamento->save()){
                    if($tt->classe == 1 || $tt->classe == '1')
                        $d = 1;   
                    else
                        $d = 2;   
                    
                    for($i = 0; $i <= $d; $i++) {
                        $tm = new TratamentoMedicamento();
                        $tm->id_test_tratamentos = $testTratamento->id;
                        $tm->st     = "0";
                        $tm->classe = $classe; 
                        $tm->save();   
                    }
                }
            }
        }else{
            TestClassifica::where(['id_classifica' => $idc, 'id_atd_test' => $idat])->delete();
            $restt = TestTratamento::where(['id_classifica' => $idc, 'id_atd_test' => $idat])->get()[0];
            TratamentoMedicamento::where(['id_test_tratamentos' => $restt->id])->delete();
            TestTratamento::where(['id_classifica' => $idc, 'id_atd_test' => $idat])->delete();
        }

        $temCla = Classifica::temClassificacao($idat);
        return response()->json($temCla);
    }

}
