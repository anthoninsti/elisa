<?php

namespace App\Http\Controllers;

use App\Models\TestTratamento;
use Illuminate\Http\Request;

class TestTratamentoController extends Controller
{
     /**
     * Retorna os teste tratamento de acondo com o parametro de id
     * Create by Adão 17-04-202*/
    public function findOne(Request $request)
    {   
        $idat = $request->idat;    
        $tm1 = TestTratamento::findByIdAtdTest($idat, "1");
        $tm2 = TestTratamento::findByIdAtdTest($idat, "2");

        return response()->json(array(
            'tm1'=> $tm1,
            'tm2'=> $tm2
        ));
    }
}
