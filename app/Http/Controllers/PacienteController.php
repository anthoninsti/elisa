<?php

namespace App\Http\Controllers;

use App\Models\AtdTest;
use App\Models\Atendimento;
use App\Models\Auxiliar;
use App\Models\Cidade;
use App\Models\Classifica;
use App\Models\Companheirx;
use App\Models\Endereco;
use App\Models\Historico;
use App\Models\Paciente;
use App\Models\Test;
use App\Models\TestTratamento;
use App\Models\Tratamento;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PacienteController extends Controller
{

    public function __construct()
    {   
        $func = auth()->guard()->user();
        
    }

    /**
     * Abre a tela para exibir a lista de paciente 
     * Create by Adão 20-03-2021
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $pacientes = Paciente::getAllPacientes();
        return view('paciente.listar',['pacientes' => $pacientes]);
    }

    /**
     * Abre a tela do parceiente
     * Create by Adão 20-03-2021 
     * @return \Illuminate\Http\Response
     */
    public function page($id_atd)
    {   
        $atdWithPac = Atendimento::getAtdWithPac($id_atd);
        $atdTests = Test::getAllTestsAtd($id_atd);
        $treatments = Paciente::seeksTreatments($atdWithPac->id_atd);
        return view('paciente.page', [
            'atd' => $atdWithPac,
            'atd_tests' => $atdTests, 
            'treatments' => $treatments
        ]);
    }

    /**
    * Abre o modal para edição dos teste de um atendimento
    * Create by Adão 11-04-2021 
    * @return \Illuminate\Http\Response
    */
    public function loadModalTeste($idp, $idat)
    {    
        $paciente = Paciente::findPacienteById($idp);
        
        $atdTest  = Test::getTestsAtdById($idat); 
        
        $classes1 = Classifica::findByClasse($idat, 1);
        $classes2 = Classifica::findByClasse($idat, 2);

        $tratamentos = Tratamento::get();
        $temCla = Classifica::temClassificacao($idat);

        $tm1 = TestTratamento::findByIdAtdTest($idat, "1");
        $tm2 = TestTratamento::findByIdAtdTest($idat, "2");
        
        return View('paciente.modal.edit_teste', [
            'paciente' => $paciente,
            'at'       => $atdTest,
            't1'       => $tratamentos[0],
            't2'       => $tratamentos[1],
            'classes1' => $classes1,
            'classes2' => $classes2,
            'temCla'   => $temCla,
            'tm1'      => $tm1,
            'tm2'      => $tm2,
            'idat'     => $idat
        ]);
    }



    /**
     * Abre a tela para cadastrar um paciente
     * Create by Adão 20-03-2021 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $cidades = Cidade::findAllCid();
        return view('paciente.create', ['cids' => $cidades]);
    }

    /**
    * Realiza o login do parciente para o aplicativo
    * Create by Adão 21-02-2021
    * @return \Illuminate\Http\Response*/
    public function loginapp(Request $request){

        $credentials = [
            'email' => $request->email,
            'senha' => $request->password,
        ];
        
        $token = Auth::guard('paciente')->attempt($credentials);
        if($token){
            $paci = Paciente::findPacienteByEmail($request->email);
            return response()->json([
                'token' => $token,
                'user' => $paci
            ]);
        }
        return response()->json([
            'token' => "",
            'user' => ""
        ]);
    }

    public function find(Request $request){
        
        $pacientes = Paciente::getPacientesLike($request->paramn);
        return response()->json([
           'pacientes' => $pacientes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paciente = new Paciente();
        $paciente->nome = $request->nome;
        $paciente->apldo = $request->apldo;
        $paciente->dt_nsc = $request->dt_nsc;
        $paciente->n_ident = $request->n_ident;
        $paciente->cpf = $request->cpf;
        $paciente->num_card_sus = $request->num_card_sus;
        $paciente->tel = $request->tel;
        $paciente->cel = $request->cel;
        $paciente->email = $request->email;
        
        $endereco = new Endereco();
        $endereco->rua    = $request->rua;
        $endereco->num    = $request->numero;
        $endereco->comple = $request->complemento;
        $endereco->id_cid = $request->id_cid;
        $endereco->bairro = $request->bairro;
        $endereco->cep    = $request->cep;
        $endereco->id_cid = $request->id_cid;
        $endereco->save();
        
        $possui = $request->possui;
        if($possui == 1){
            $companheirx = new Companheirx();
            $companheirx->nome = $request->nome2;
            $companheirx->apldo = $request->apldo2;
            $companheirx->dt_nsc = $request->dt_nsc2;
            $companheirx->save();
            $paciente->id_companheiro = $companheirx->id;            
        }

        $paciente->id_endrc = $endereco->id;
        $paciente->save();

        //Criação do atendimento
        $atendimento = new Atendimento();
        $atendimento->id = date("YmdHis");
        $atendimento->data = date("Y-m-d H:i:s");
        $atendimento->st = 1;
        $atendimento->id_func = 1;
        $atendimento->id_parc = $paciente->id;
        $atendimento->save();

        $his = new Historico();
        $his->id_pac = $paciente->id;
        $his->atividade = "Cadastro do paciente";
        $his->descri = "Foi realizado o cadastro do paciente";
        $his->dt = date("d/m/Y-m-d h:i:s");
        $his->st = "1";
        
        $res = array(
            'status' => 1, 
            'msg' => "Paciente cadastrado com sucesso!"
        ); 
           
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $pac = Paciente::findAllInfoById( $id);
        $cidades = Cidade::findAllCid();
        return view('paciente.edit', ['pac'=>$pac, 'cids' => $cidades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        
        $pacParam = array(
            'nome'     => $request->nome_pac,
            'apldo' => $request->apldo_pac,
            'dt_nsc' => $request->dt_nsc_pac,
            'n_ident' => $request->n_ident_pac,
            'cpf' => $request->cpf_pac,
            'num_card_sus' => $request->num_card_sus_pac,
            'tel' => $request->tel,
            'cel' => $request->cel,
            'email' => $request->email,
            'sexo' => $request->sexo_pac,
            'obs' => $request->obs,
        );

        $resUdtPac = Paciente::where('id', $request->id_pac)->update($pacParam);

        $compParam = array(
            'nome'    => $request->nome_comp,
            'apldo'   => $request->apldo_comp,
            'dt_nsc'  => $request->dt_nsc_comp,
            'cpf'     => $request->cpf_comp,
            'num_card_sus' => $request->num_card_sus_comp,
            'sexo'    => $request->sexo_comp,
        );

        if($request->possui){
            if($request->id_comp != null){
                $resUdtComp = Companheirx::where('id', $request->id_comp)->update($compParam);
            }else{
                $comp = Companheirx::create($compParam);
                Paciente::where('id', $request->id_pac)->update(['id_companheiro' => $comp->id]);
            }
        }else{
            $pac = Paciente::findPacienteById($request->id_pac);
            
            //verifica se o paciente tem companheiro para deleta-lo
            if($pac->id_companheiro != null){
                Companheirx::where('id', $pac->id_companheiro)->delete();
                Paciente::where('id', $request->id_pac)->update(['id_companheiro' => null]);
            }
        }

        $endParam = array(
            'rua' => $request->rua,
            'num' => $request->num,
            'comple' => $request->comple,
            'bairro' => $request->bairro,
            'cep' => $request->cep,
            'id_cid' => $request->id_cid,
        );

        $resUdtPac = Endereco::where('id', $request->id_end)->update($endParam);

        $res = array(
            'status' => true,
            'msg' => "Informações editadas com sucesso!",
        );
        
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $res = [];
        $pac = Paciente::where('id', $id)->first();
        
        if($pac->id_endrc != null){
            array_push($res, array(
                'resend' => Endereco::where('id', $pac->id_endrc)->delete()
            ));
        }
        
        if($pac->id_companheiro != null){
            array_push($res, array(
                'rescomp' => Companheirx::where('id', $pac->id_companheiro)->delete()
            ));
        }

        array_push($res, array(
            'resatd' => Atendimento::where('id_parc', $id)->delete()
        ));

        array_push($res, array(
            'respac' => $pac->delete()
        ));

        $result = array(
            'status' => true,
            'msg' => "Informações deletada com sucesso!"
        );
        return response()->json($result);
    }
}
