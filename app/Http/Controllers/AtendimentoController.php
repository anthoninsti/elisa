<?php

namespace App\Http\Controllers;

use App\Models\AtdTest;
use App\Models\Atendimento;
use App\Models\Paciente;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

class AtendimentoController extends Controller
{
    public function __construct()
    {   
        $func = auth()->guard();
    }

    /**
     * Abre a tela para exibir uma lista de atendimentos
     * Create by Adão 04.04.2021
     * @return \Illuminate\Http\Response
     */
    public function index($date="")
    {   

        if($date == "all"){
            $atendimentos = Atendimento::getAllAtendimentos();
        }else{
            if($date == ""){
                $date = date('Y-m-d');
            }
            
            $atendimentos = Atendimento::getAtdimentosByDate($date);
        }
        
        return view('atendimento.listar',['atendimentos' => $atendimentos]);
    }

    public function loadModal($id, $id_atd)
    {   
        $testes1 = AtdTest::getAllTestsAtd($id_atd);
        $testes2 = AtdTest::getAllTestsAtd($id_atd);
        $paciente = Paciente::findPacienteById($id);
        $atd = Atendimento::where('id', $id_atd)->first();
        
        return View('atendimento.modal.create_test',[
            'paciente' => $paciente,
            'obs'      => $atd->obs,
            'testes1'  => $testes1,
            'testes2'  => $testes2,
            'id_atd'   => $id_atd]);
    }

    /**
    * Cria os testes do atendimento
    * Creates the service tests
    * @return \Illuminate\Http\Response
    */
    public function storeTestesAtd(Request $request)
    {   $obs = $request->obs;
        $testes = $request->testes;
        $id_atd = $request->id_atd;

        //Deleta todos os testes de uma atendimento
        AtdTest::delAllTestsAtd($id_atd);

        //Cria todos o testes de um atendiemento
        AtdTest::createAllTestsAtd($id_atd, $testes);

        //Salva observações do atendimento
        $atd = Atendimento::where('id', $id_atd)->first();
        $atd->obs = $obs;
        $atd->save();

        $res = array(
            'status' => 1, 
            'msg' => "Testes cadastrados com sucesso!"
        );

        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
