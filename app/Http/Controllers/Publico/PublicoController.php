<?php

namespace App\Http\Controllers\Publico;

use App\Http\Controllers\Controller;
use App\models\CatTab;
use Illuminate\Http\Request;
use App\Models\Config;
use App\Models\Home\Dev\Post;
use App\Models\Home\Dev\Socio;
use App\models\Home\HomeAgencia;
use App\models\RecCatTab;
use Exception;

class PublicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $string = file_get_contents(public_path().'\configuracao.json');
        $config = json_decode($string, true);
        
        $tabs    = $config["tabs"];
        $banner  = $config["banner"];        
        $agencia = $config["agencia"];        

        return view('publico.index', [
            'banner'  => $banner,
            'tabs'    => $tabs,
            'agencia' => $agencia
        ]);
    }

    /**
    *Função usada para as informações do vip club 
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function vipclub(){
        
        return view('publico.quem_somos.vipclub');
    }
    // empresarial 
    public function empresarial(){
        return view('publico.quem_somos.grupoempresarial');
    }
     //cana brava resort
     public function bravaresort(){
        return view('publico.quem_somos.canabravaresort');
    }
     //Equipe
     public function equi(){
        return view('publico.quem_somos.equipe');
    }
    
    /*********************************************************************************
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function progpontos()
    {
        return view('publico.clube_ferias.programapontos');
    }
     /**
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function  rc(){
        return view('publico.clube_ferias.clubeferiasrci');

    }
     /**
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function allinclusive(){
        return view('publico.clube_ferias.all-inclusive');

    }
     /**
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function dayuse(){
        return view('publico.clube_ferias.day_use');

    }
    /**
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function sharing(){
        return view('publico.clube_ferias.time_sharing');

    }
     /**
    *Função usada para as informações do vip club pontos
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function restaura(){
        return view('publico.clube_ferias.restaurante');

    }
    //***************************************************************************** */
     /**
    *Função usada para as informações do vip club,vip fidelidade
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function fidelidade(){
        return view ('publico.vip.fidelidade');

    }
    /***************************************************************************************
     *Função usada para as informações do vip club PET
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function pet(){
        return view ('publico.pet.pet');
    }
    
    /***************************************************************************************
    *Função usada para as informações do vip club  Espaço kids
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    //Espaço kids
    public function kidscrianca(){
         return view('publico.espacokids.casadacrianca');
    }
    /*Função usada para as informações do vip club  Espaço kids
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    //Espaço kids
    public function monitore(){
        return view('publico.espacokids.monitores');
    }
    
    /*Função usada para as informações do vip club  Espaço kids
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    //Espaço kids
    public function jog(){
        return view('publico.espacokids.jogos');
    }

    /****************************************************************************************
    *Função usada para as informações do vip club  Espaço kids
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function acomodar(){
        return view('publico.acomodacoes.acomodacao');
    }

    /****************************************************************************************
    *Função usada para as informações do vip club  Espaço kids
    *create by giovani 21/12/2020
    * @return \Illuminate\Http\Response
    */
    public function contato(){
        return view('publico.contatos.contato');
    }
}
