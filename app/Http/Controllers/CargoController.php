<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use Illuminate\Http\Request;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $cargos = Cargo::all();
        return view('cargo.listar', ['cargos'=> $cargos]);
    }

    /**
     * Abre a tela para cria um cargo
     * Create by Adão 22-03-2021
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cargo.create');
    }

    /**
     * Cadastra um cargo
     * Create by Adão 25-03-2021
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $cargo = new Cargo();
        $cargo->nome = $request->nome;
        $cargo->st = 1;

        $res = array(
            'status' => $cargo->save(), 
            'msg' => "Cargo criado com sucesso!"
           ); 
           
        return response()->json($res);
    }

    /**
    * Modifica o status do cargo
    * Create by Adão 25-03-2021
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function alterStatus(Request $request)
    {   
        $st = $request->st == "true" ? 1 : 0;
        $s = Cargo::where('id', $request->id)->update(['st' => $st]);
        
        $res = array(
            'id' => $request->id,
            'msg' => "Cargo criadxzxo com sucesso!"
        ); 
           
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
