<?php

namespace App\Http\Controllers;

use App\Models\Auxiliar;
use App\Models\Cargo;
use App\Models\Funcionario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;

class FuncionarioController extends Controller
{
    
    public function login()
    {
        return view('funcionario.login');
    }

    public function loginStore()
    {
        return view('funcionario.login');
    }
    
    /**
     * Abre a tela para exibir uma lista de funcionarios
     * Create by Adão 22-03-2021
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $funcionarios = Funcionario::getAllFuncionarios();
        return view('funcionario.listar', ['funcionarios' => $funcionarios]);
    }

    /**
     * Abre a tela para cadastrar um novo funcionário
     * Create by Adão 22-03-2021
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = Cargo::all();
        return view('funcionario.create', ['cargos' => $cargos]);
    }

    /**
     * Método usado para cadastrar um novo funcionário
     * Create by Adão 04.04.2021
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $funcionario = new Funcionario();
        $funcionario->nome = $request->nome;
        $funcionario->username = $request->apldo;
        $funcionario->n_ident = $request->n_ident;
        $funcionario->email = $request->email;
        $funcionario->password = Hash::make("elisa1234");
        
        $funcionario->cpf = $request->cpf;
        $funcionario->sexo = Auxiliar::formatSexoString($request->sexo);
        $funcionario->st = 1;
        $funcionario->create_at = date("Y-m-d H:i:s");
        $funcionario->id_cargo = $request->id_cargo;
        $res = $funcionario->save();
        
        if($res){
            $res = array(
                'status' => 1, 
                'msg' => "Funcionário cadastrado com sucesso!"
            );
        }else{
            $res = array(
                'status' => 0, 
                'msg' => "Falha ao cadastrar Funcionário !"
            );
        }

        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $func = Funcionario::getFuncWithCargo($id);
        return View('funcionario.show', ['func' => $func]);
    }

    /* Abre o modal para edição do funcionario
     * Create by Adão 20/04/2021*/
    public function edit($id)
    {   
        $cargos = Cargo::all();
        $func = Funcionario::getFuncWithCargo($id);
        return View('funcionario.edit', ['cargos' => $cargos, 'func' => $func]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $params = array(
            'nome'     => $request->nome,
            'username' => $request->apldo,
            'n_ident'  => $request->n_ident,
            'email'    => $request->email,
            'cpf'      => $request->cpf,
            'sexo'     => Auxiliar::formatSexoString($request->sexo),  
            'id_cargo' => $request->id_cargo,
        );
        
        if(Funcionario::where('id', $id)
            ->update($params)){
            $msg = array(
                'status' => true,
                'msg'    => "Informações editadas com sucesso!"
            );
        }else{
            $msg = array(
                'status' => false,
                'msg'    => "Falha ao editar informações!"
            );
        }
    
        return response()->json($msg);
    }

    /* Deleta um funcionario de acordo com o id 
     * Create by Adão 20/04/2021*/
    public function destroy(Request $request)
    {   
        $id  = $request->id;
        
        if(Funcionario::where('id',$id)->delete()){
            $msg = array(
                'status' => true,
                'msg'    => "Funcionario deletado com sussesso!"
            );
        }else{
            $msg = array(
                'status' => false,
                'msg'    => "Falha ao deletar funcionario!"
            );
        }    
        return response()->json($msg);
    }
}
