<?php

namespace App\Http\Controllers;

use App\Models\AtdTest;
use App\Models\Service;
use App\Models\TratamentoMedicamento;
use Illuminate\Http\Request;

class TestController extends Controller
{
    
    /**
     * Alter status dos test 
     * Create by Adão 12-04-2021
     * @return \Illuminate\Http\Response
     */
    public function alterResult(Request $request)
    {
        $res_test = $request->result;
        
        Service::sendResultTestFCM("ElisaApp", "Resultado do teste disponível", "eN9cG6zoHxM:APA91bHC2VEhQNFy_QGdxLAyokm7O0jSgah2x8QUNam5Q_AuGkFkqw62qltlHbOi31svRiaHlBH1CaZaVPEmMqZZ-pPAgzf2_24BUap-vDcQLYCM186VmaOeEYUZLYkK5AsS40vpXENz");

        $res = AtdTest::updateResultTest($request->id_atd_test, $res_test);   
        return response()->json($res);
    }

    /**
    * Altera os atributos dos testes
    * Create by Adão 12-04-2021*/
    public function alter(Request $request)
    {   
        $t1 = $request->t1;
        $t2 = $request->t2;
        
        if($t1 != null)
            TratamentoMedicamento::updateAllTratMed($t1);
        
        if($t2 != null)
            TratamentoMedicamento::updateAllTratMed($t2);

        return response()->json($t2);
    }

}
 