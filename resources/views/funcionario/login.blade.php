<!--================================================================================
	Item Name: Elisa Login
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
    ================================================================================ -->

    <!DOCTYPE html>
    <html lang="pt-br">
    
    <head>
        <meta charset="utf-8" />
        <title>ElisaApp</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('socio/image/icons/icon.png')}}">

        <!-- third party css -->
        <link href="{{asset('socio/assets/css/vendor/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->

        <!-- App css -->
        <link href="{{asset('socio/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('socio/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" id="light-style" />
        

    </head>
    <body class="authentication-bg pb-0">
        @php 
            $img  = asset('img/banner/backgound.jpg');
            $logo = asset('img/banner/bannertopo.jpg');
        @endphp
        <div class="auth-fluid" style="background: url(<?= $img; ?>) center;">
            <!--Auth fluid left content -->
            <div class="auth-fluid-form-box">
                <div class="align-items-center d-flex h-100">

                    <!--------------------------------- login admin----------------------------------------------------------->
                    <div class="card-body login-admin">
                        <!-- Logo -->
                        <div class=" text-center text-lg-left">
                            <a href="#">
                                <span><img src="{{asset('socio/image/icons/logo2.png')}}" style="margin-top: -98px;"; alt="" width="400px"></span>
                            </a>
                        </div>

                        <!-- title-->
                        <h4 class="mt-0">Login</h4>
                        <p class="text-muted mb-4">Entre com seu usuario e sua senha para acessar sua conta.</p>

                        <!-- form -->
                        <form method="POST" action="{{ route('funcionario.login.store') }}">
                            @csrf

                            <fieldset class="form-group position-relative has-icon-left">
                                <input id="email" type="email" class="form-control form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Digite seu e-mail">
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input id="password" type="password" class=" form-control form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Digite sua senha">
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-md-6 col-12 text-center text-sm-left">
                                </div>
                                <div class="col-md-6 col-12 float-sm-left text-center text-sm-right"><a href="" class="card-link">Esqueceu a senha?</a></div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary btn-block">{{ __('Login') }}</button>
                            </div>
                        </form>
                        <!-- Footer-->
                        <footer class="footer footer-alt">
                            <p class="text-muted" style="display:none;">Não tem conta? <a href="pages-register-2.html" class="text-muted ml-1"><b>click aqui</b></a></p>
                        </footer>
                    </div> <!-- end .card-body -->
                    <!----------------------------------------- fim login admi------------------------------------------------>
                </div> <!-- end .align-items-center.d-flex.h-100-->
            </div>
            <!-- end auth-fluid-form-box-->

            <!-- Auth fluid right content -->
            <div class="auth-fluid-right text-center">
                <div class="auth-user-testimonial">
                    <h2 class="mb-3">Medical Control </h2>
                    <p class="lead"><i class="mdi mdi-format-quote-open"></i>Cuidando de vidas<i class="mdi mdi-format-quote-close"></i>
                    </p>
                </div> <!-- end auth-user-testimonial-->
            </div>
            <!-- end Auth fluid right content -->
        </div>
        <!-- end auth-fluid-->

        <!-- bundle -->
        <script src="socio/assets/js/vendor.min.js"></script>
        <script src="socio/assets/js/app.min.js"></script>
    </body>
</html>