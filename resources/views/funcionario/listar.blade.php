<!--================================================================================
	Item Name: Funcnionário - listar
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
   

    

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <h4 class="page-title">Lista de Funcionários</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Info Header Modal -->
                    <div id="info-header-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="info-header-modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div id="container" class="modal-content">
                           
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="row-callback-preview">
                                        <table id="row-callback-datatable" class="table-funcionarios table dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Username</th>
                                                    <th>Nome</th>
                                                    <th>E-mail</th>
                                                    <th>Cargo</th>
                                                    <th>Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($funcionarios as $funcionario)
                                                    <tr>
                                                        <td>{{$funcionario->nome_func}}</td>
                                                        <td>{{$funcionario->username}}</td>
                                                        <td>{{$funcionario->email}}</td>
                                                        <td>{{$funcionario->nome_carg}}</td>
                                                        <td class="table-action" >
                                                            <a  class="icon-show-paciente action-icon" data-id="{{$funcionario->id_func}}" data-toggle="modal" data-target="#info-header-modal"> <i class="mdi mdi-eye"></i></a>
                                                            <a  class="icon-edt-paciente action-icon" data-id="{{$funcionario->id_func}}" data-toggle="modal" data-target="#info-header-modal"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                            <a  class="icon-del-paciente action-icon" data-id="{{$funcionario->id_func}}"><i class="mdi mdi-delete"></i></a>
                                                        </td>
                                                    </tr> 
                                                @endforeach                                               
                                            </tbody>        
                                        </table>                                          
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div><!-- end row-->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script src="{{asset('paciente/js/listar.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('socio/assets/js/pages/demo.datatable-init.js')}}"></script>
    <script>
        delPaciente = function(data){
            Swal.fire({
            title: 'Deletar funcionario?',
                text: "Você não poderá reverter isso!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!'
            }).then((result) => {  
                if (result.isConfirmed) { 
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('funcionario.destroy') }}",
                        data: data,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        beforeSend: function() {
                            Swal.fire({
                                title: "Processando...",
                                imageUrl: "{{ asset('img/loading.gif') }}",
                                showConfirmButton: false,
                            });
                        },
                        success: function(data){
                            if(data.status){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sucesso!',
                                    text: data.msg
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })     
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Falha ao excluir funcionario'
                                })
                            }
                        }
                    });
                }
            })
        }
        
        $('.table-funcionarios tbody').on('click', '.icon-del-paciente', function() {
            delPaciente({id:$(this).data('id')})
        });
        
        //Editar funcionario
        $('.table-funcionarios tbody').on('click', '.icon-edt-paciente', function() {
            var id = $(this).data('id')
            console.log(id)
            url = "funcionario/edit/" + id
            $.get(base_url + url, function( data ) {
                $('#container').html(data)
            });
        });

        //Visualizar funcionario
        $('.table-funcionarios tbody').on('click', '.icon-show-paciente', function() {
            var id = $(this).data('id')
            console.log(id)
            url = "funcionario/show/" + id
            $.get(base_url + url, function( data ) {
                $('#container').html(data)
            });
        });

    </script>
@endsection



