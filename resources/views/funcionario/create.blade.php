<!--================================================================================
	Item Name: Funcnionário - create
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                        <i class="mdi mdi-autorenew"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Cadastro Funcionário</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="vertical-left-tabs-preview">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade active show" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="card ribbon-box">
                                                                    <div class="card-body">
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane show active" id="form-row-preview">
                                                                                <form>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="nome" class="col-form-label">Nome</label>
                                                                                            <input id="nome" type="text" class="form-control">
                                                                                            <span id="enome" class="help-block erro"><small>informe o nome do funcionário.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="apldo" class="col-form-label">Apelido</label>
                                                                                            <input id="apldo" type="text" class="form-control" >
                                                                                           <!-- <span id="epldo" class="help-block erro"><small>informe o apleido</small></span> -->
                                                                                        </div>
                                                                                        <div class="form-group col-md-12">
                                                                                            <label for="email" class="col-form-label">E-mail</label>
                                                                                            <input id="email" type="text" class="form-control" >
                                                                                            <span id="eemail" class="help-block erro"><small>informe o e-mail</small></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="n_ident" class="col-form-label">Nº identidade</label>
                                                                                            <input id="n_ident" type="text" class="form-control">
                                                                                            <!-- <span id="en_ident" class="help-block erro"><small>informe o número da identidade</small></span> -->
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="cpf" class="col-form-label">CPF</label>
                                                                                            <input id="cpf" type="mail" class="form-control">
                                                                                            <span id="ecpf" class="help-block erro"><small>informe o CPF.</small></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="sexo" class="col-form-label">Sexo</label>
                                                                                            <select id="sexo" class="form-control">
                                                                                                <option>Selecione a opção</option>
                                                                                                <option>Feminino</option>
                                                                                                <option>Masculino</option>
                                                                                                <option>Não definido</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="cargo" class="col-form-label">Cargo</label>
                                                                                            <select id="cargo" class="form-control">
                                                                                                <option>Selecione a opção</option>
                                                                                                @foreach($cargos as $cargo)                     
                                                                                                    <option data-id="{{$cargo->id}}">{{$cargo->nome}}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group col-md-4">
                                                                                        <button id="btn-send" type="button" class="btn btn-primary">Enviar</button>
                                                                                        <button id="btn-send" type="button" class="btn btn-primary">Cancelar</button>
                                                                                    <div class="form-group col-md-12">
                                                                                </form>                      
                                                                            </div> <!-- end preview-->
                                                                        </div> <!-- end tab-content-->
                                                                    </div> <!-- end card-body -->
                                                                </div> <!-- end card-->
                                                            </div> <!-- end col -->
                                                        </div>
                                                        <!-- end row -->
                                                    </div>
                                                </div> <!-- end tab-content-->
                                            </div> <!-- end col-->
                                        </div>
                                        <!-- end row-->                                            
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card-body -->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script>
        var config = {
            routes: {
                store: "{{ route('funcionario.store') }}",
            }
        };
    </script>
    <script src="{{asset('funcionario/js/create.js')}}"></script>
@endsection
<!--End JS section-->


