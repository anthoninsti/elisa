<!--================================================================================
	Item Name: Visualizar - Funcionário
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->

<div class="modal-header modal-colored-header bg-info">
    <h4 class="modal-title" id="info-header-modalLabel">Visualizar Funcionario</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <h6 class="font-15">Informações pessoais</h6>
            <div class="mt-3">
                <label for="id">Nome: {{$func->nome_func}}</label>
                <br><span><small>Username: {{$func->username}}</small></span><br>    
                <br><span><small>Nº Identidade: {{$func->n_ident}}</small></span><br>    
                <br><span><small>CPF: {{$func->cpf}}</small></span><br>    
                <br><span><small>Sexo: {{$func->sexo}}</small></span><br>    
                <hr>
            </div>

            <h6 class="font-15">Contato</h6>
            <div class="mt-3">
                <label for="id">{{$func->email}}</label>
                <hr>
            </div>

            <h6 class="font-15">Informações profissionais</h6>
            <div class="mt-3">
                <label for="id">{{$func->nome_carg}}</label>
                <hr>
            </div>
        </div>
    </div>
</div>
