<!--================================================================================
	Item Name: Editar - Funcionario
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->

<div class="modal-header modal-colored-header bg-info">
    <h4 class="modal-title" id="info-header-modalLabel">Editar Funcionario</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="nome" class="col-form-label">Nome</label>
                <input id="nome" type="text" class="form-control" value="{{$func->nome_func}}">
                <span id="enome" class="help-block erro"><small>informe o nome do funcionário.</small></span>
            </div>
            <div class="form-group col-md-6">   
                <label for="apldo" class="col-form-label">Apelido</label>
                <input id="apldo" type="text" class="form-control" value="{{$func->username}}">
                <!-- <span id="epldo" class="help-block erro"><small>informe o apleido</small></span> -->
            </div>
            <div class="form-group col-md-12">
                <label for="email" class="col-form-label">E-mail</label>
                <input id="email" type="text" class="form-control" value="{{$func->email}}">
                <span id="eemail" class="help-block erro"><small>informe o e-mail</small></span>
            </div>
        </div>
        
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="n_ident" class="col-form-label">Nº identidade</label>
                <input id="n_ident" type="text" class="form-control" value="{{$func->n_ident}}">
                <!-- <span id="en_ident" class="help-block erro"><small>informe o número da identidade</small></span> -->
            </div>
            <div class="form-group col-md-6">
                <label for="cpf" class="col-form-label">CPF</label>
                <input id="cpf" type="mail" class="form-control" value="{{$func->cpf}}">
                <span id="ecpf" class="help-block erro"><small>informe o CPF.</small></span>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="sexo" class="col-form-label">Sexo</label>
                <select id="sexo" class="form-control">
                    <option>Selecione a opção</option>
                    <option value="F" {{$func->sexo == "F" ? "selected" : ""}}>Feminino</option>
                    <option value="M" {{$func->sexo == "M" ? "selected" : ""}}>Masculino</option>
                    <option value="I" {{$func->sexo == "I" ? "selected" : ""}}>Não definido</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="cargo" class="col-form-label">Cargo</label>
                <select id="cargo" class="form-control">
                    <option>Selecione a opção</option>
                    @foreach($cargos as $cargo)                     
                        <option data-id="{{$cargo->id}}" {{$cargo->id == $func->id_cargo ? "selected" : ""}}>{{$cargo->nome}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </form>     
</div>
<div class="modal-footer">
    <button id="btn-edit" type="button" class="btn btn-primary">Enviar</button>
    <button  type="button" class="btn btn-primary">Cancelar</button>
</div>
<script src="{{asset('funcionario/js/create.js')}}"></script>
<script>
    $(document).ready(function() {

        updatePac = function(data){
            Swal.fire({
            title: 'Atualizar informações?',
                text: "Você não poderá reverter isso!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, Atualizar!'
            }).then((result) => {  
                if (result.isConfirmed) { 
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('funcionario.update', $func->id_func) }}",
                        data: data,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        beforeSend: function() {
                            Swal.fire({
                                title: "Processando...",
                                imageUrl: "{{ asset('img/loading.gif') }}",
                                showConfirmButton: false,
                            });
                        },
                        success: function(data){
                            console.log(data)
                            if(data.status){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sucesso!',
                                    text: data.msg
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })     
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Falha ao excluir funcionario'
                                })
                            }
                        }
                    });
                }
            })
        }

        //Evento botão salvar
        $('#btn-edit').click(function() { 
            console.log(getData())
            if(!verifyempty()){
                updatePac(getData())
            }
        });
    });
</script>