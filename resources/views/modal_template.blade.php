<!--================================================================================
	Item Name: Editar - Funcionario
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->

<div class="modal-header">
    <h4 class="modal-title" id="standard-modalLabel">Editar Funcionario</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
   
  
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
    <button id="btn-send" type="button" class="btn btn-primary">Salvar</button>
</div>

<script>
    $(document).ready(function() {
    });
</script>