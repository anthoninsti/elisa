<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <script>document.write(new Date().getFullYear())</script> © Sistema - Elisa
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-md-block">
                    <a href="javascript: void(0);">Sobre</a>
                    <a href="javascript: void(0);">soporte</a>
                    <a href="javascript: void(0);">Contato</a>
                </div>
            </div>
        </div>
    </div>
</footer>
@csrf
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">

    $('.logout').click(function(){

        var _token = $('[name=_token]').val()
        console.log(_token)
        $.ajax({
            url: "{{ route('logout') }}",
            type: 'POST',
            data: {_token:_token},
            dataType: 'json',
        }).done(function(response) { 
            window.location.href = ""
            console.log(response)
        });

    })
</script>