<div class="h-100" id="left-side-menu-container" data-simplebar>
    <ul class="metismenu side-nav" style="margin-top: 53px;">
        <li class="side-nav-title side-nav-item">Navegação</li>
        <li class="side-nav-item">
            <a href="#" class="side-nav-link">
                <i class="uil-home-alt"></i>
                <span>Home </span>
            </a>
        </li>
        <li class="side-nav-item">
            <a href="{{route('atendimento.listar')}}" class="side-nav-link">
                <i class="uil-home-alt"></i>
                <span>Atendimentos </span>
            </a>
        </li>
        <li class="side-nav-item">
            <a href="javascript: void(0);" class="side-nav-link">
                <i class="uil-box"></i>
                <span>Paciente </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="side-nav-second-level mm-collapse" aria-expanded="false">
                <li>
                    <a href="{{route('paciente.listar')}}">Visualizar</a>
                </li>
                <li>
                    <a href="{{route('paciente.create')}}">Cadastrar</a>
                </li>
            </ul>
        </li>
        <li class="side-nav-item mm-active">
            <a href="javascript: void(0);" class="side-nav-link" aria-expanded="true">
                <i class="uil-folder-plus"></i>
                <span>Administrativo </span>
                <span class="menu-arrow"></span>
            </a>
            <ul class="side-nav-second-level mm-collapse mm-show" aria-expanded="false">
                <li class="side-nav-item">
                    <a href="javascript: void(0);" aria-expanded="false">Funcionário
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-third-level mm-collapse" aria-expanded="false" style="height: 0px;">
                        <li>
                            <a href="{{route('funcionario.listar')}}">Visualizar</a>
                        </li>
                        <li>
                            <a href="{{route('funcionario.create')}}">Cadastrar</a>
                        </li>
                    </ul>
                </li>
                <li class="side-nav-item">
                    <a href="javascript: void(0);" aria-expanded="false">Cargo
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="side-nav-third-level mm-collapse" aria-expanded="false">
                        <li>
                            <a href="{{route('cargo.listar')}}">Visualizar</a>
                        </li>
                        <li>
                            <a href="{{route('cargo.create')}}">Cadastrar</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="side-nav-title side-nav-item">Opções</li>
        <li>
            <a href="#" class="side-nav-link">
                <i class="uil-store"></i>
                <span>Lista de Postos</span>
            </a>
        </li>
        <li>
            <a href="#" class="side-nav-link">
                <i class="uil-store"></i>
                <span>Gia de procedimentos</span>
            </a>
        </li>
        <li>
            <a href="#" class="side-nav-link">
                <i class="uil-store"></i>
                <span>Ajuda</span>
            </a>
        </li>
    </ul>
    <div class="clearfix"></div>
</div>