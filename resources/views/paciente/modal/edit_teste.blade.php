
<div class="modal-header">
    <h4 class="modal-title" id="standard-modalLabel">Acompanhamento teste</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <h6 id="atd" data-id_atd_test="{{$at->id_atds}}" data-result="{{$at->res}}">{{$paciente->nome}}</h6>
    <p>{{$paciente->email}}</p>
    <hr>
    <h4 class="modal-title" id="standard-modalLabel">{{$at->nome_test}}</h4>
    <div class="mt-2">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="cr1" name="res_teste" class="custom-control-input res_teste" value="2" {{ $at->res == 2 ? 'checked' : '' }}>
            <label class="custom-control-label" for="cr1">Processando</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="cr2" name="res_teste" class="custom-control-input res_teste" value="1" {{ $at->res == 1 ? 'checked' : '' }}>
            <label class="custom-control-label" for="cr2">Positivo</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="cr3" name="res_teste" class="custom-control-input res_teste" value="0" {{ $at->res == 0 ? 'checked' : '' }}>
            <label class="custom-control-label" for="cr3">Negativo</label>
        </div>
    </div>
    <hr>
    <div id="container">
        <h4>Classificação</h4>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-pane show active" id="checkbox-preview">
                    <h6 class="font-15">Sífilis Recente.</h6>
                    <div class="mt-3">
                        <input id="ipt1"  type="text" class="form-control" placeholder="99/99/9999" value="testando">
                        @foreach($classes1 as $cl)
                            <div class="custom-control custom-checkbox line">
                                <input id="{{$cl->id.'teste'}}" data-id="{{$cl->id}}" data-type="2" data-classe="{{$cl->classe}}" type="checkbox" class="checkcl custom-control-input" {{ $cl->tem_test ? 'checked' : '' }}>
                                <label class="custom-control-label pcbx" for="{{$cl->id.'teste'}}" style="padding-left: 23px">{{$cl->nome}}</label>
                                <br><span class="help-block"><small>{{$cl->descicao}}</small></span><br>    
                            </div>
                            <hr>
                        @endforeach
                    </div>

                    <h6 class="font-15 mt-3">Sífilis Tardia</h6>

                    <div class="mt-3">
                        @foreach($classes2 as $cl)
                            <div class="custom-control custom-checkbox line">
                                <input id="{{$cl->id.'teste'}}" data-id="{{$cl->id}}" data-type="2" data-classe="{{$cl->classe}}" type="checkbox" class="checkcl custom-control-input" {{ $cl->tem_test ? 'checked' : '' }}>
                                <label class="custom-control-label pcbx" for="{{$cl->id.'teste'}}" style="padding-left: 23px">{{$cl->nome}}</label>
                                <br><span class="help-block"><small>{{$cl->descicao}}</small></span><br>    
                            </div>
                            <hr>
                        @endforeach
                    </div>

                </div> 
            </div>
        </div>
        <h4>Tratamento</h4>
        <div id="container-t1">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-pane show active" id="checkbox-preview">
                        <h6 class="font-15">{{$t1->tipo}}</h6>
                        <p>{{$t1->procedimento}}</p>
                        <p><b>{{$t1->ocorrencia}}</b></p>   
                        <div id="container-m1">                      
                           
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <hr>
        <div id="container-t2">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-pane show active" id="checkbox-preview">
                        <h6 class="font-15">{{$t2->tipo}}</h6>
                        <p>{{$t2->procedimento}}</p>
                        <p><b>{{$t2->ocorrencia}}</b></p>
                        <div id="container-m2">
                           
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
    <button id="btn-send" type="button" class="btn btn-primary">Salvar</button>
</div>

<script>
    $(document).ready(function() {

        $('.dt').mask("99/99/9999");

        //Altera o status do teste
        alterStatusTest = function(dados) {
            $.ajax({
                type: 'POST',
                url: "{{ route('teste.alter.result') }}",
                data: dados,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    console.log(data)
                }
            });
        }

        alterClassifica = function(dados) {
            $.ajax({
                type: 'POST',
                url: "{{ route('classifica.create_or_destroy') }}",
                data: dados,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    showAndHideT1(data.cls1)
                    showAndHideT2(data.cls2)
                }
            });
        }

        getTratamentos  = function(dados) {
            $.ajax({
                type: 'POST',
                url: "{{ route('testtratamento.find_one') }}",
                data: dados,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    var linhas = ''
                    for (var i = 0; i < data.tm1.length; i++) {
                        var s1 = data.tm1[i].st == '1' ? 'selected' : '';
                        var s2 = data.tm1[i].st == '2' ? 'selected' : '';
                        var s3 = data.tm1[i].st == '3' ? 'selected' : '';
                        var v = data.tm1[i].dt != null ? data.tm1[i].dt : '';
                        var ids = "s1" + i;
                        
                        linhas += `
                        <div class="form-row line1">
                            <div class="form-group col-md-6">
                                <label for="`+ data.tm1[i].id +`" class="col-form-label">`+ (i + 1) +`º dose</label>
                                <input id="`+ data.tm1[i].id +`" data-id="`+ data.tm1[i].id +`"  type="text" class="dt form-control" placeholder="99/99/9999" value="`+ v +`">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="` + ids + `" class="col-form-label">Status</label>
                                <select id="` + ids + `" class="st form-control">
                                    <option value="0">Selecione a opção</option>
                                        <option ` + s1 + ` value="1">Agendaddo</option>
                                        <option ` + s2 + ` value="2">Paciente tomou a dose</option>
                                        <option ` + s3 + ` value="3">Paciente tomou não dose</option>
                                </select>
                            </div>
                        </div>`   
                    }
                    $('#container-m1').html(linhas)
                    linhas = ''
                    for (var i = 0; i < data.tm2.length; i++) {
                        var s1 = data.tm2[i].st == '1' ? 'selected' : '';
                        var s2 = data.tm2[i].st == '2' ? 'selected' : '';
                        var s3 = data.tm2[i].st == '3' ? 'selected' : '';
                        var v = data.tm2[i].dt != null ? data.tm2[i].dt : '';
                        var ids = "s2" + i;

                        linhas += `
                        <div class="form-row line2">
                            <div class="form-group col-md-6">
                                <label for="`+ data.tm2[i].id +`" class="col-form-label">`+ (i + 1) +`º dose</label>
                                <input id="`+ data.tm2[i].id +`" data-id="`+ data.tm2[i].id +`" type="date" class="dt form-control" placeholder="99/99/9999" value="`+ v +`">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="` + ids + `" class="col-form-label">Status</label>
                                <select id="` + ids + `" class="st form-control">
                                    <option value="0">Selecione a opção</option>
                                    <option ` + s1 + ` value="1">Agendaddo</option>
                                    <option ` + s2 + ` value="2">Paciente tomou a dose</option>
                                    <option ` + s3 + ` value="3">Paciente tomou não dose</option>
                                </select>
                            </div>
                        </div>`   
                    }
                    $('#container-m2').html(linhas)
                }
            });
        }

        showAndHide = function(paramn){
            if(paramn == 1){
                $('#container').show() 
            }else{
                $('#container').hide() 
            }
        }

        showAndHideT1 = function(paramn){
            if(paramn == 1){
                
                $('#container-t1').show() 
            }else{
                $('#container-t1').hide() 
            }
        }

        showAndHideT2 = function(paramn){
            if(paramn == 1){
                $('#container-t2').show()
            }else{
                $('#container-t2').hide() 
            }
        }

        //Evento altera o status do teste
        $('.res_teste').change(function(){
            var result =  $(this).val()
            showAndHide(result)
            alterStatusTest({
                id_atd_test:$('#atd').data('id_atd_test'),
                result:result
            })
        })

        //Evento altera o status da classificacão
        $('.checkcl').change(function(){
            var res = $(this).is(':checked')
            var id_atd_test = $('#atd').data('id_atd_test')
            var id_classifica = $(this).data('id')
            var classe = $(this).data('classe')

            alterClassifica({
                r:res,
                idat:id_atd_test,
                idc:id_classifica,
                classe:classe
            })
        })

        getValueTrat = function(line){
            var trat = [];
            line.each(function() { 
                var slt = $(this).children().children(".st")
                trat.push({
                    idt:$(this).children().children(".dt").data("id"),
                    dt:$(this).children().children(".dt").val(),
                    st:$(slt, 'option:selected').val()
                });
            });
            return trat;
        }

        //Envia as datas dos testes para o controller
        sendTest = function(dados) {
            $.ajax({
                type: 'POST',
                url: "{{ route('teste.alter') }}",
                data: dados,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(data) {
                    console.log(data)
                }
            });
        }
        
        //Oculta linha classificação e tratamento
        showAndHide($('#atd').data('result'))
        showAndHideT1("{{$temCla->cls1}}")
        showAndHideT2("{{$temCla->cls2}}")
        getTratamentos({idat:"{{$idat}}"}) 

        $('#btn-send').click(function() { 
            sendTest({
                t1:getValueTrat($(".line1")),
                t2:getValueTrat($(".line2")),
            }) 
        });

    });
</script>