<!--================================================================================
	Item Name: Editar - Paciente
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->

<div class="modal-header modal-colored-header bg-info">
    <h4 class="modal-title" id="info-header-modalLabel">Editar Paciente</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <form>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="nome" class="col-form-label">Nome</label>
                <input id="nome" type="text" class="form-control" value="{{$pac->nome_pac}}">
                <span id="enome" class="help-block erro"><small>informe o nome do paciente.</small></span>
            </div>
            <div class="form-group col-md-6">
                <label for="apldo" class="col-form-label">Apelido</label>
                <input id="apldo" type="text" class="form-control" value="{{$pac->apldo_pac}}">
                <span id="eapldo" class="help-block erro"><small>informe o apelido</small></span>
            </div>
            <div class="form-group col-md-6">
                <label for="dt_nsc" class="col-form-label">Data de Nascimento</label>
                <input id="dt_nsc" type="text" class="dt form-control" value="{{$pac->dt_nsc_pac}}">
                <span id="edt_nsc" class="help-block erro"><small>Informe a Data de Nascimento</small></span>
            </div>
        </div>
        
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="sexo_pac" class="col-form-label">Sexo</label>
                <select id="sexo_pac" class="form-control">
                    <option>Selecione a opção</option>
                    <option value="M" {{$pac->sexo_pac=="M" ? "selected":""}}>Masculino</option>
                    <option value="F" {{$pac->sexo_pac=="F" ? "selected":""}}>Feminino</option>
                    <option value="F" {{$pac->sexo_pac=="I" ? "selected":""}}>Indefinido</option>
                </select>
                <span id="esexo" class="help-block erro"><small>Selecione o sexo.</small></span>
            </div>
            <div class="form-group col-md-6">
                <label for="num_card_sus" class="col-form-label">Nº Cartão SUS</label>
                <input id="num_card_sus" type="mail" class="form-control" value="{{$pac->num_cad_sus_pac}}">
            </div>
            <div class="form-group col-md-6">
                <label for="n_ident" class="col-form-label">Nº identidade</label>
                <input id="n_ident" type="text" class="form-control" value="{{$pac->n_ident_pac}}">
            </div>
            <div class="form-group col-md-6">
                <label for="cpf" class="col-form-label">CPF</label>
                <input id="cpf" type="mail" class="cpf form-control" value="{{$pac->cpf_pac}}">
                <span id="ecpf" class="help-block erro"><small>informe o CPF.</small></span>
            </div>
        </div>
        <div class="mt-2">
            <div class="custom-control custom-radio custom-control-inline">
                Possui companheiro(a)?
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadio1" name="possui-companheirx" class="custom-control-input possui-companheirx" value="1" {{$pac->nome_comp != null ? "checked" : ""}}>
                <label class="custom-control-label" for="customRadio1">Sim</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="customRadio2" name="possui-companheirx" class="custom-control-input possui-companheirx" value="0" {{$pac->nome_comp == null ? "checked" : ""}}>
                <label class="custom-control-label" for="customRadio2">Não</label>
            </div>
        </div>
        
        <div id="line-partner"class="form-row">
            <div class="form-group col-md-6">
                <label for="nome2" class="col-form-label">Nome</label>
                <input id="nome2" type="text" class="form-control required" value="{{$pac->nome_comp}}">
                <span id="enome2" class="help-block erro"><small>informe o nome do Companheiro.</small></span>
            </div>
            <div class="form-group col-md-6">
                <label for="apldo" class="col-form-label">Apelido</label>
                <input id="apldo2" type="text" class="form-control" value="{{$pac->apldo_comp}}">
            </div>
            <div class="form-group col-md-6">
                <label for="sexo_comp" class="col-form-label">Sexo</label>
                <select id="sexo_comp" class="form-control">
                    <option>Selecione a opção</option>
                    <option value="M" {{$pac->sexo_comp=="M" ? "selected":""}}>Masculino</option>
                    <option value="F" {{$pac->sexo_comp=="F" ? "selected":""}}>Feminino</option>
                    <option value="I" {{$pac->sexo_comp=="I" ? "selected":""}}>Indefinido</option>
                </select>
                <span id="esexo_comp" class="help-block erro"><small>Selecione o sexo.</small></span>
            </div>
            <div class="form-group col-md-6">
                <label for="dt_nsc2" class="col-form-label">Data de Nascimento</label>
                <input id="dt_nsc2" type="text" class="dt form-control" value="{{$pac->dt_nsc_comp}}">
            </div>
        </div>

        <div id="line_after_comp" class="form-row">
            <div class="form-group col-md-4">
                <label for="tel" class="col-form-label">Telefone</label>
                <input id="tel" type="phone" class="tel form-control" value="{{$pac->tel}}">
                <span id="etel" class="help-block erro"><small>informe o Nº do Telefone.</small></span>

            </div>
            <div class="form-group col-md-4">
                <label for="cel" class=" col-form-label">Celular</label>
                <input id="cel" type="phone" class="cel form-control" value="{{$pac->tel}}">
                <span id="ecel" class="help-block erro"><small>informe o Nº do celular.</small></span>
            </div>
            <div class="form-group col-md-4">
                <label for="email" class="col-form-label">E-mail</label>
                <input id="email" type="mail" class="form-control" value="{{$pac->email}}">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="rua" class="col-form-label">Rua</label>
                <input id="rua" type="mail" class="form-control" value="{{$pac->rua}}">
                <span id="erua" class="help-block erro"><small>informe a rua.</small></span>
            </div>
            <div class="form-group col-md-4">
                <label for="num" class="col-form-label">Número</label>
                <input id="num" type="text" class="form-control" value="{{$pac->num}}">
                <span id="enum" class="help-block erro"><small>informe o número.</small></span>
            </div>
            <div class="form-group col-md-4">
                <label for="comple" class="col-form-label">Complemento</label>
                <input id="comple" type="text" class="form-control" value="{{$pac->comple}}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="bairro" class="col-form-label">Bairro</label>
                <input id="bairro" type="text" class="form-control" value="{{$pac->bairro}}">
                <span id="ebairro" class="help-block erro"><small>informe o bairro.</small></span>
            </div>
            <div class="form-group col-md-4">
                <label for="cidade" class="col-form-label">Cidade</label>
                <select id="cidade" class="form-control">
                    <option>Selecione a opção</option>
                    @foreach($cids as $cid)
                        <option data-id="{{$cid->id}}" {{$pac->id_cid == $cid->id ? "selected" : ""}}>{{$cid->nome}}</option>
                    @endforeach;
                </select>
                <span id="ecidade" class="help-block erro"><small>Selecione a cidade.</small></span>
            </div>
            <div class="form-group col-md-4">
                <label for="cep" class="col-form-label">CEP</label>
                <input id="cep" type="mail" class="cep form-control" value="{{$pac->cep}}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="obs">Deseja fazer alguma observação?</label>
                <textarea id="obs" class="form-control" rows="5"></textarea>
                </div>
        </div>
    </form>  
</div>
<div class="modal-footer">
    <button id="btn-edit" type="button" class="btn btn-primary">Enviar</button>
    <button  type="button" class="btn btn-primary">Cancelar</button>
</div>

<script src="{{asset('funcionario/js/create.js')}}"></script>
<script>
    var possui = 1;

    $(document).ready(function() {
        $('.cel').mask("(99) 99999-9999");
        $('.dt').mask("99/99/9999");
        $('.cpf').mask("999.999.99-99");
        $('.tel').mask("(99) 9999-9999");
        $('.cep').mask("99999-999");

        updatePac = function(data){
            Swal.fire({
            title: 'Atualizar informações?',
                text: "Você não poderá reverter isso!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, Atualizar!'
            }).then((result) => {  
                if (result.isConfirmed) { 
                    $.ajax({
                        type: 'POST',
                        url: '{{route("paciente.update")}}',
                        data: data,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        beforeSend: function() {
                            Swal.fire({
                                title: "Processando...",
                                imageUrl: "{{ asset('img/loading.gif') }}",
                                showConfirmButton: false,
                            });
                        },
                        success: function(data){
                            console.log(data.status)
                            if(data.status){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sucesso!',
                                    text: data.msg
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })     
                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Falha ao excluir funcionario'
                                })
                            }
                        }
                    });
                }
            })
        }

       //Pega os dados digitados nos campos
        getData = function(){
            var sexo_pac = "";
            var sexo_comp = "";
            
            if($('#sexo_pac').val() != "Selecione a opção"){
                sexo_pac = $('#sexo_pac').val()
            }

            if($('#sexo_comp').val() != "Selecione a opção"){
                sexo_comp = $('#sexo_comp').val()
            }
                
            return{
                id_pac:'{{$pac->id_pac}}', 
                nome_pac:$('#nome').val(),
                apldo_pac:$('#apldo').val(),
                dt_nsc_pac:$('#dt_nsc').val(),
                sexo_pac:sexo_pac,
                n_ident_pac:$('#n_ident').val(),
                cpf_pac:$('#cpf').val(),
                num_card_sus_pac:$('#num_card_sus').val(),
                possui:possui,
                id_comp:'{{$pac->id_comp}}',
                nome_comp:$('#nome2').val(),
                apldo_comp:$('#apldo2').val(),
                sexo_comp:sexo_comp,
                dt_nsc_comp:$('#dt_nsc2').val(),
                tel:$('#tel').val(),   
                cel:$('#cel').val(), 
                email:$('#email').val(),
                id_end:'{{$pac->id_end}}',
                rua:$('#rua').val(),   
                num:$('#num').val(),
                comple:$('#comple').val(),
                id_cid:$('#cidade option:selected').data("id"),
                bairro:$('#bairro').val(),
                cep:$('#cep').val(),
                obs:$('#obs').val()
            }
        }

        initialPossuiCompanheiro = function(){
            var nome_comp = '{{$pac->nome_comp}}'
            if(nome_comp != null && nome_comp != ""){
                possui = 1;
                $('#line_after_comp').css('margin-top','0px')
                $('#line-partner').css({"visibility": "initial"});
            }else{
                possui = 0;
                $('#line_after_comp').css('margin-top','-206px')
                $('#line-partner').css({"visibility": "hidden"});
            }
        }

        //Evento quando selecionando a quantidade de hospedes 
        $('.possui-companheirx').change(function(){
            var st = $(this).val() 
            if(st == 1){
                possui = 1;
                $('#line_after_comp').css('margin-top','0px')
                $('#line-partner').css({"visibility": "initial"});
                
            }else{
                possui = 0;
                $('#line_after_comp').css('margin-top','-206px')
                $('#line-partner').css({"visibility": "hidden"});
            }   
        });

        //Evento botão salvar
        $('#btn-edit').click(function() { 
            updatePac(getData())
        });

        initialPossuiCompanheiro()
    });
</script>