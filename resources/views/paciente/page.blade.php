<!--================================================================================
	Item Name: Paciente - Page
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')

    <!-- Attachment Modal -->
    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content load_modal">
                
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                        <i class="mdi mdi-autorenew"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Dados do Paciente</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class="dripicons-document float-right" style="color:#cacdf9;"></i>
                                <h6 class=" text-uppercase mt-0">Atendimento</h6>
                                <h2 class="m-b-20">{{$atd->id_atd}}</h2>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class=" dripicons-hourglass float-right" style="color:#cacdf9;"></i>
                                <h6 class=" text-uppercase mt-0">Data Atendimento</h6>
                                <h2 class="m-b-20">{{$atd->data}}</h2>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->

                    <div class="col-sm-4">
                        <div class="card tilebox-one">
                            <div class="card-body">
                                <i class="dripicons-calendar float-right" style="color:#cacdf9;"></i>
                                <h6 class=" text-uppercase mt-0">status</h6>
                                <h2 class="m-b-20">{{$atd->st}}</h2>
                            </div> <!-- end card-body-->
                        </div> <!--end card-->
                    </div><!-- end col -->
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="card text-center">
                            <div class="card-body">
                            
                                <img src="{{asset('assets/images/icons/user.png')}}" class="rounded-circle avatar-lg img-thumbnail"
                                alt="profile-image">

                                <h4 class="mb-0 mt-2">{{$atd->nome_pac}} ({{$atd->apldo}})</h4>
                                
                                <h5 class="mb-1">Paciente</h5>
                                
                                <div
                                 class="text-left mt-3">
                                    <p class=" mb-2 font-13"><strong>Num SUS: </strong> <span class="ml-2">{{$atd->num_card_sus}}</span></p>
                                    <p class=" mb-2 font-13"><strong>CPF: </strong> <span class="ml-2">{{$atd->cpf}}</span></p>
                                    <p class=" mb-2 font-13"><strong>RG: </strong><span class="ml-2">{{$atd->n_ident}}</span></p>
                                    <p class=" mb-2 font-13"><strong>Data Nascimento: </strong> <span class="ml-2 ">{{$atd->dt_nsc}}</span></p>
                                    <p class=" mb-1 font-13"><strong>Profissão: </strong> <span class="ml-2"></span></p>
                                </div>

                                <ul class="social-list list-inline mt-3 mb-0">
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
                                                class="mdi mdi-facebook"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                                class="mdi mdi-google"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                                class="mdi mdi-twitter"></i></a>
                                    </li>
                                </ul>
                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div> <!-- end col-->
                    <div class="col-xl-6 col-lg-6">
                        <div class="card text-center">
                            <div class="card-body">
                                <img src="{{asset('assets/images/icons/user.png')}}" class="rounded-circle avatar-lg img-thumbnail"
                                alt="profile-image">

                                <h4 class="mb-0 mt-2">Nome</h4>
                                <h5 class="mb-1">Companheiro</h5>
                                
                                <div class="text-left mt-3">
                                    <p class=" mb-2 font-13"><strong>Num SUS :</strong> <span class="ml-2">{{$atd->cnum_card_sus}}</span></p>
                                    <p class=" mb-2 font-13"><strong>CPF :</strong> <span class="ml-2">{{$atd->ccpf}}</span></p>
                                    <p class=" mb-2 font-13"><strong>RG :</strong><span class="ml-2"></span></p>
                                    <p class=" mb-2 font-13"><strong>Data Nascimento :</strong> <span class="ml-2 ">{{$atd->cdt_nsc}}</span></p>
                                    <p class=" mb-1 font-13"><strong>Profissão :</strong> <span class="ml-2"></span></p>
                                </div>

                                <ul class="social-list list-inline mt-3 mb-0">
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
                                                class="mdi mdi-facebook"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                                class="mdi mdi-google"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                                class="mdi mdi-twitter"></i></a>
                                    </li>
                                </ul>
                            </div> <!-- end card-body -->
                        </div> <!-- end card -->
                    </div> <!-- end col-->
                </div>
                <div class="row">
                    <div class="col-xl-12  col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="dropdown float-right">
                                        <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                            <i class="mdi mdi-dots-vertical"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                            <!-- item-->
                                            <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                        </div>
                                    </div>
                                    <h4 class="header-title mb-3">Informações</h4>
                                    <h4 class="mt-1 mb-1 ">Telefones:</h4>
                                    <ul class="mb-0 list-inline ">
                                    
                                            <li class="list-inline-item mr-3">
                                                <i class="dripicons-phone float-left" style="margin-right:10px; color:#cacdf9;"></i>
                                                <h7 class="mb-1">{{$atd->cel}} / {{$atd->tel}}</h7>
                                            </li>
                                        
                                    </ul>  
                                    <h4 class="mt-1 mb-1 ">E-mails:</h4>
                                    <ul class="mb-0 list-inline ">
                                        
                                            <li class="list-inline-item mr-3">
                                                <i class="dripicons-mail float-left" style="margin-right:10px; color:#cacdf9;"></i>
                                                <h7 class="mb-1">{{$atd->email}}</h7>
                                            </li>
                                        
                                    </ul>
                                    <h4 class="mt-1 mb-1 ">Endereço:</h4>
                                    <ul class="mb-0 list-inline ">
                                        <li class="list-inline-item mr-3">
                                            <i class="dripicons-location float-left" style="margin-right:10px; color:#cacdf9;"></i>
                                            <h7 class="mb-1">{{$atd->rua}}, Nº{{$atd->num}}, Complemento: {{$atd->comple}}, {{$atd->cid_nome}} - {{$atd->estado}}    
                                            </h7>
                                        </li>
                                    </ul>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div> <!-- end col -->
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xl-4 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="dropdown float-right">
                                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Sales Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Export Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Profit</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                    </div>
                                </div>
                                <h4 class="header-title mb-2">Relatório de atividades</h4>

                                <div data-simplebar style="max-height: 424px;">
                                    <div class="timeline-alt pb-0">
                                        <div class="timeline-item">
                                            <i class="mdi mdi-upload bg-info-lighten text-info timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-info font-weight-bold mb-1 d-block">You sold an item</a>
                                                <small>Paul Burgess just purchased “Hyper - Admin Dashboard”!</small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">5 minutes ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-airplane bg-primary-lighten text-primary timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-primary font-weight-bold mb-1 d-block">Product on the Bootstrap Market</a>
                                                <small>Dave Gamache added
                                                    <span class="font-weight-bold">Admin Dashboard</span>
                                                </small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">30 minutes ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-microphone bg-info-lighten text-info timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-info font-weight-bold mb-1 d-block">Robert Delaney</a>
                                                <small>Send you message
                                                    <span class="font-weight-bold">"Are you there?"</span>
                                                </small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">2 hours ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-upload bg-primary-lighten text-primary timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-primary font-weight-bold mb-1 d-block">Audrey Tobey</a>
                                                <small>Uploaded a photo
                                                    <span class="font-weight-bold">"Error.jpg"</span>
                                                </small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">14 hours ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-upload bg-info-lighten text-info timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-info font-weight-bold mb-1 d-block">You sold an item</a>
                                                <small>Paul Burgess just purchased “Hyper - Admin Dashboard”!</small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">16 hours ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-airplane bg-primary-lighten text-primary timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-primary font-weight-bold mb-1 d-block">Product on the Bootstrap Market</a>
                                                <small>Dave Gamache added
                                                    <span class="font-weight-bold">Admin Dashboard</span>
                                                </small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">22 hours ago</small>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="timeline-item">
                                            <i class="mdi mdi-microphone bg-info-lighten text-info timeline-icon"></i>
                                            <div class="timeline-item-info">
                                                <a href="#" class="text-info font-weight-bold mb-1 d-block">Robert Delaney</a>
                                                <small>Send you message
                                                    <span class="font-weight-bold">"Are you there?"</span>
                                                </small>
                                                <p class="mb-0 pb-2">
                                                    <small class="text-muted">2 days ago</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end timeline -->
                                </div> <!-- end slimscroll -->
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card-->
                    </div>
                    <div class="col-xl-8 col-lg-12">
                        <!-- Standard modal content -->
                        <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div id="conteudo" class="modal-content">
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <div class="dropdown float-right">
                                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Weekly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Monthly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Settings</a>
                                    </div>
                                </div>
                                <h4 class="header-title mb-3">Testes</h4>

                                <p><b>{{sizeof($atd_tests)}}</b> Testes solicitados</p>

                                <div class="table-responsive">
                                    <table class="table table-centered table-nowrap table-hover mb-0">
                                        <tbody>
                                            @foreach($atd_tests as $at):
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="{{'cr'.$at->id_atd_test}}" data-id="{{$at->id_atd_test}}" name="possui-companheirx" class="custom-control-input tip_test" checked>
                                                            <label class="custom-control-label" for="{{'cr'.$at->id_atd_test}}"></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body">{{"Sífilis ".$at->nome_test}}</a></h5>
                                                        <span class="text-muted font-13">Solicitado em 11/04/2021</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-muted font-13">Status</span> <br/>
                                                        @if($at->res == '0')
                                                            <span class="badge badge-outline-danger badge-pill">Negativo</span>
                                                        @elseif($at->res == '1')
                                                            <span class="badge badge-outline-success badge-pill">Positivo</span>
                                                        @else
                                                            <span class="badge badge-outline-warning badge-pill">Em análise</span>
                                                        @endif
                                                        
                                                    </td>
                                                    <td>
                                                        <span class="text-muted font-13">Classificação</span>
                                                        <h5 class="font-14 mt-1 font-weight-normal">3h 20min</h5>
                                                    </td>
                                                    <td>
                                                        <span class="text-muted font-13">Solicitante</span>
                                                        <h5 class="font-14 mt-1 font-weight-normal">Dr Paulo Carqueija</h5>
                                                    </td>
                                                    
                                                    <td class="table-action" style="width: 90px;">
                                                        <a class="action-icon edit_test" data-toggle="modal" data-target="#standard-modal" data-id="{{$at->id_pac}}" data-id_test="{{$at->id_atd_test}}"> <i class="mdi mdi-pencil"></i></a>
                                                        <a class="action-icon del_test" data-id="{{$at->id_atd_test}}"> <i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach   
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive-->

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="dropdown float-right">
                                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Weekly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Monthly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Settings</a>
                                    </div>
                                </div>
                                <h4 class="header-title mb-4">Status do tratamento</h4>

                                <div class="my-4 chartjs-chart" style="height: 202px;">
                                    <canvas id="project-status-chart" data-colors="#0acf97,#727cf5,#fa5c7c"></canvas>
                                </div>

                                <div class="row text-center mt-2 py-2">
                                    <div class="col-4">
                                        <i class="mdi mdi-trending-up text-success mt-3 h3"></i>
                                        <h3 class="font-weight-normal">
                                            <span>64%</span>
                                        </h3>
                                        <p class="text-muted mb-0">Completo</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="mdi mdi-trending-down text-primary mt-3 h3"></i>
                                        <h3 class="font-weight-normal">
                                            <span>26%</span>
                                        </h3>
                                        <p class="text-muted mb-0">Em progresso</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="mdi mdi-trending-down text-danger mt-3 h3"></i>
                                        <h3 class="font-weight-normal">
                                            <span>10%</span>
                                        </h3>
                                        <p class="text-muted mb-0">Finalizado</p>
                                    </div>
                                </div>
                                <!-- end row-->

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->

                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="dropdown float-right">
                                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Weekly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Monthly Report</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Settings</a>
                                    </div>
                                </div>
                                <h4 class="header-title mb-3">Tratamentos</h4>
                                @if($treatments != null)                                                    
                                    <p>{{$treatments[0]->procedimento}}</p>
                                @endif

                                <div class="table-responsive">
                                    <table class="table table-centered table-nowrap table-hover mb-0">
                                        <tbody>
                                            @foreach($treatments as $i => $treatent)
                                                <tr>
                                                    <td>
                                                        <h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body">{{$i + 1}}</a></h5>
                                                    </td>
                                                    <td>
                                                        <h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body">{{$treatent->tipo}}</a></h5>
                                                        <span class="text-muted font-13">Em tratamento</span>
                                                    </td>
                                                    <td>
                                                        <span class="text-muted font-13">Data medicação</span>
                                                        <h5 class="font-14 mt-1 font-weight-normal">{{$treatent->dt}}</h5>
                                                    </td>
                                                    <td>
                                                        <span class="text-muted font-13">Status</span> <br/>
                                                        <span class="badge badge-danger-lighten">Outdated</span>
                                                    </td>
                                                    <td class="table-action" style="width: 90px;">
                                                        <a href="javascript: void(0);" class="action-icon"> <i class="mdi mdi-pencil"></i></a>
                                                        <a href="javascript: void(0);" class="action-icon"> <i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div> <!-- end table-responsive-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
    <style>
       .dataTables_filter{
            margin-left: 21em;
       } 
    </style>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script src="{{asset('paciente/js/page.js')}}"></script>
    <script>
        
       
    </script>
    
@endsection
<!--End JS section-->


