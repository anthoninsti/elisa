<!--================================================================================
	Item Name: Página cadastro do paciente
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                        <i class="mdi mdi-autorenew"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Cadastro paciente</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="vertical-left-tabs-preview">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade active show" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="card ribbon-box">
                                                                    <div class="card-body">
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane show active" id="form-row-preview">
                                                                                <form>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="nome" class="col-form-label">Nome</label>
                                                                                            <input id="nome" type="text" class="form-control">
                                                                                            <span id="enome" class="help-block erro"><small>informe o nome do paciente.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="apldo" class="col-form-label">Apelido</label>
                                                                                            <input id="apldo" type="text" class="form-control" >
                                                                                            <span id="eapldo" class="help-block erro"><small>informe o apelido</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="dt_nsc" class="col-form-label">Data de Nascimento</label>
                                                                                            <input id="dt_nsc" type="text" class="dt form-control">
                                                                                            <span id="edt_nsc" class="help-block erro"><small>Informe a Data de Nascimento</small></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="n_ident" class="col-form-label">Nº identidade</label>
                                                                                            <input id="n_ident" type="text" class="form-control">
                                                                                            <!--<span id="en_ident" class="help-block erro"><small>informe o número da identidade</small></span> -->
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="cpf" class="col-form-label">CPF</label>
                                                                                            <input id="cpf" type="mail" class="cpf form-control">
                                                                                            <span id="ecpf" class="help-block erro"><small>informe o CPF.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="num_card_sus" class="col-form-label">Nº Cartão SUS</label>
                                                                                            <input id="num_card_sus" type="mail" class="form-control">
                                                                                            <!--<span id="num_card_sus" class="help-block erro"><small>informe o Nº do cartão do SUS.</small></span> -->
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="mt-2">
                                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                                            Possui companheiro(a)?
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                                            <input type="radio" id="customRadio1" name="possui-companheirx" class="custom-control-input possui-companheirx" value="1" checked>
                                                                                            <label class="custom-control-label" for="customRadio1">Sim</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio custom-control-inline">
                                                                                            <input type="radio" id="customRadio2" name="possui-companheirx" class="custom-control-input possui-companheirx" value="0">
                                                                                            <label class="custom-control-label" for="customRadio2">Não</label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="line-partner"class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="nome2" class="col-form-label">Nome</label>
                                                                                            <input id="nome2" type="text" class="form-control">
                                                                                            <span id="enome2" class="help-block erro"><small>informe o nome do Companheiro.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="apldo" class="col-form-label">Apelido</label>
                                                                                            <input id="apldo2" type="text" class="form-control" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="dt_nsc2" class="col-form-label">Data de Nascimento</label>
                                                                                            <input id="dt_nsc2" type="text" class="dt form-control" >
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="tel" class="col-form-label">Telefone</label>
                                                                                            <input id="tel" type="phone" class="tel form-control">
                                                                                            <span id="etel" class="help-block erro"><small>informe o Nº do Telefone.</small></span>

                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="cel" class=" col-form-label">Celular</label>
                                                                                            <input id="cel" type="phone" class="cel form-control">
                                                                                            <span id="ecel" class="help-block erro"><small>informe o Nº do celular.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="email" class="col-form-label">E-mail</label>
                                                                                            <input id="email" type="mail" class="form-control">
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="rua" class="col-form-label">Rua</label>
                                                                                            <input id="rua" type="mail" class="form-control">
                                                                                            <span id="erua" class="help-block erro"><small>informe a rua.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="num" class="col-form-label">Número</label>
                                                                                            <input id="num" type="text" class="form-control">
                                                                                            <span id="enum" class="help-block erro"><small>informe o número.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="comple" class="col-form-label">Complemento</label>
                                                                                            <input id="comple" type="text" class="form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="bairro" class="col-form-label">Bairro</label>
                                                                                            <input id="bairro" type="text" class="form-control">
                                                                                            <span id="ebairro" class="help-block erro"><small>informe o bairro.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="cidade" class="col-form-label">Cidade</label>
                                                                                            <select id="cidade" class="form-control">
                                                                                                <option>Selecione a opção</option>
                                                                                                @foreach($cids as $cid)
                                                                                                    <option data-id="{{$cid->id}}">{{$cid->nome}}</option>
                                                                                                @endforeach;
                                                                                            </select>
                                                                                            <span id="ecidade" class="help-block erro"><small>Selecione a cidade.</small></span>
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="cep" class="col-form-label">CEP</label>
                                                                                            <input id="cep" type="mail" class="cep form-control">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-12">
                                                                                            <label for="obs">Deseja fazer alguma observação?</label>
                                                                                            <textarea id="obs" class="form-control" rows="5"></textarea>
                                                                                         </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <button id="btn-send" type="button" class="btn btn-primary">Enviar</button>
                                                                                            <button id="btn-send" type="button" class="btn btn-primary">Cancelar</button>
                                                                                        <div class="form-group col-md-12">
                                                                                    </div>
                                                                                </form>                      
                                                                            </div> <!-- end preview-->
                                                                        </div> <!-- end tab-content-->
                                                                    </div> <!-- end card-body -->
                                                                </div> <!-- end card-->
                                                            </div> <!-- end col -->
                                                        </div>
                                                        <!-- end row -->
                                                    </div>
                                                </div> <!-- end tab-content-->
                                            </div> <!-- end col-->
                                        </div>
                                        <!-- end row-->                                            
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card-body -->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    
    <script>
        $('.cel').mask("(99) 99999-9999");
        $('.dt').mask("99/99/9999");
        $('.cpf').mask("999.999.99-99");
        $('.tel').mask("(99) 9999-9999");
        $('.cep').mask("99999-999");
        
        var config = {
            routes: {
                store: "{{ route('paciente.store') }}",
            }
        };
    </script>
    <script src="{{asset('paciente/js/create.js')}}"></script>
@endsection
<!--End JS section-->


