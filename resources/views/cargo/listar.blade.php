<!--================================================================================
	Item Name: Cargo - listar
	Version: 1.0	
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                        <i class="mdi mdi-autorenew"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Lista de Cargo</h4>
                        </div>
                    </div>
                </div>
                <div class="row">

                <!-- Attachment Modal -->
                <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit-modal-label">Edit Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="attachment-body-content">
                        <form id="edit-form" class="form-horizontal" method="POST" action="">
                        <div class="card text-white bg-dark mb-0">
                            <div class="card-header">
                            <h2 class="m-0">Edit</h2>
                            </div>
                            <div class="card-body">
                            <!-- id -->
                            <div class="form-group">
                                <label class="col-form-label" for="modal-input-id">Id (just for reference not meant to be shown to the general public) </label>
                                <input type="text" name="modal-input-id" class="form-control" id="modal-input-id" required>
                            </div>
                            <!-- /id -->
                            <!-- name -->
                            <div class="form-group">
                                <label class="col-form-label" for="modal-input-name">Name</label>
                                <input type="text" name="modal-input-name" class="form-control" id="modal-input-name" required autofocus>
                            </div>
                            <!-- /name -->
                            <!-- description -->
                            <div class="form-group">
                                <label class="col-form-label" for="modal-input-description">Email</label>
                                <input type="text" name="modal-input-description" class="form-control" id="modal-input-description" required>
                            </div>
                            <!-- /description -->
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
                </div>
                <!-- /Attachment Modal -->
                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="row-callback-preview">
                                        <table id="row-callback-datatable" class="table dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($cargos as $cargo)
                                                    <tr>
                                                        <td>{{$cargo->nome}}</td>
                                                        <td>
                                                            <input class="cbx_cargo" type="checkbox" id="{{ $cargo->id}}" data-id="{{ $cargo->id }}" data-switch="bool" {{ $cargo->st == 1 ? 'checked':'' }}>
                                                            <label for="{{ $cargo->id}}" data-on-label="On" data-off-label="Off">
                                                        </td>
                                                        <td class="table-action" >
                                                            <a href="javascript:void(0);" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                                                            <a href="javascript:void(0);" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                            <a href="javascript:void(0);" class="action-icon"> <i class="mdi mdi-delete"></i></a>
                                                        </td>
                                                    </tr>                                                
                                                @endforeach
                                            </tbody>
                                        </table>                                          
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div><!-- end row-->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script>
        var  edit = "";
        var  alter_status = "{{ route('cargo.alter.status') }}";
        
    </script>
    <script src="{{asset('cargo/js/listar.js')}}"></script>
    
    <script src="{{asset('socio/assets/js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.buttons.min.js')}}"></script>
    
    <script src="{{asset('socio/assets/js/pages/demo.datatable-init.js')}}"></script>
@endsection
<!--End JS section-->


