<!--================================================================================
	Item Name: Cargo - create
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
<link href="{{asset('socio/assets/css/estilo.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('socio/assets/css/pages/reserva_cana.css')}}" rel="stylesheet" type="text/css"/>
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-primary border-primary text-white">
                                                    <i class="mdi mdi-calendar-range font-13"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-2">
                                        <i class="mdi mdi-autorenew"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Cadastro Cargo</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="vertical-left-tabs-preview">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade active show" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="card ribbon-box">
                                                                    <div class="card-body">
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane show active" id="form-row-preview">
                                                                                <form>
                                                                                    <div class="form-row">
                                                                                        <div class="form-group col-md-12">
                                                                                            <label for="nome" class="col-form-label">Nome</label>
                                                                                            <input id="nome" type="text" class="form-control">
                                                                                            <span id="enome" class="help-block erro"><small>informe o cargo.</small></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                                <div class="form-group col-md-4">
                                                                                    <button id="btn-send" type="button" class="btn btn-primary">Enviar</button>
                                                                                    <button id="btn-send" type="button" class="btn btn-primary">Cancelar</button>
                                                                                <div class="form-group col-md-12">                      
                                                                            </div> <!-- end preview-->
                                                                        </div> <!-- end tab-content-->
                                                                    </div> <!-- end card-body -->
                                                                </div> <!-- end card-->
                                                            </div> <!-- end col -->
                                                        </div>
                                                        <!-- end row -->
                                                    </div>
                                                </div> <!-- end tab-content-->
                                            </div> <!-- end col-->
                                        </div>
                                        <!-- end row-->                                            
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card-body -->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script>
        var config = {
            routes: {
                store: "{{ route('cargo.store') }}",
            }
        };
    </script>
    <script src="{{asset('cargo/js/create.js')}}"></script>
@endsection
<!--End JS section-->


