<!DOCTYPE html>
<html lang="pt-br">

<!--================================================================================
	Item Name: VIPCLUBSI - Admin socio page
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  <title>Vibclub SI - Implementando Soluções</title>

  <!-- Favicons-->
  <link rel="icon" href="../../../assets/images/logo/icon-logo.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="../../../assets/images/logo/icon-white-vipclubsi.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="../../../assets/images/logo/icon-white-vipclubsi.png">
  <!-- For Windows Phone -->

  <!-- CORE CSS-->
  <link href="../../../materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../../materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->    
  <link href="../../../materialize/css/custom-style.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../../materialize/css/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="../../../materialize/css/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="../../../materialize/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  
</head>

<body class="cyan">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form class="login-form" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row">
            <div class="input-field col s12 center">
                <h4>Cadastrar</h4>
            </div>
            </div>
            <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-social-person-outline prefix"></i>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                <label for="name" class="center-align">Username</label>
                
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            </div>
            <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-communication-email prefix"></i>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                <label for="email" class="center-align">Email</label>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            </div>
            <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-action-lock-outline prefix"></i>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                <label for="password">Senha</label>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            </div>
            <div class="row margin">
            <div class="input-field col s12">
                <i class="mdi-action-lock-outline prefix"></i>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                <label for="password-again">Repetir Senha</label>
                
            </div>
            </div>
            <div class="row">
            <div class="input-field col s12">
                <button type="submit" class="btn btn-primary">
                    {{ __('Salvar') }}
                </button>
            </div>
            <div class="input-field col s12">
                <p class="margin center medium-small sign-up">Já tem Conta? <a href="page-login.html">Login</a></p>
            </div>
            </div>
        </form>
    </div>
  </div>



  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="../../../materialize/js/jquery-1.11.2.min.js"></script>
  <!--materialize js-->
  <script type="text/javascript" src="../../../materialize/js/materialize.js"></script>
  <!--prism-->
  <script type="text/javascript" src="../../../materialize/js/prism.js"></script>
  <!--scrollbar-->
  <script type="text/javascript" src="../../../materialize/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

  <!--plugins.js - Some Specific JS codes for Plugin Settings-->
  <script type="text/javascript" src="../../../materialize/js/plugins.js"></script>

</body>


<!-- Mirrored from demo.geekslabs.com/materialize/v2.1/layout01/user-register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 16:34:00 GMT -->
</html>