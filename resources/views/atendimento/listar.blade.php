<!--================================================================================
	Item Name: Atendimento - listar
	Version: 1.0
	Author: Adão
	Mail: jose.carlos.adao@hotmail.com
================================================================================ -->
@extends('layouts.template')

<!--Body section-->
@section('content')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control form-control-light" id="dash-daterange">
                                            <div class="input-group-append btn-pesquisar-data">
                                                <span class="input-group-text bg-primary border-primary text-white btn-pesquisar-data">
                                                    <i class="mdi mdi-magnify search-icon btn-pesquisar-data"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('atendimento.listar', 'all')}}" class="btn btn-primary ml-2">
                                        <i class="dripicons-list"></i>
                                    </a>
                                    <a href="javascript: void(0);" class="btn btn-primary ml-1">
                                        <i class="mdi mdi-filter-variant"></i>
                                    </a>
                                </form>
                            </div>
                            <h4 class="page-title">Atendimentos </h4>
                        </div>
                    </div>
                </div>
                <div class="row">

                <!-- Attachment Modal -->
                <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content load_modal">
                           
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!-- /Attachment Modal -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane show active" id="row-callback-preview">
                                        <table id="row-callback-datatable" class="table dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Nº Atd</th>
                                                    <th>Nome</th>
                                                    <th>E-mail</th>
                                                    <th>Teste</th>
                                                    <th></th>
                                                    <th>Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($atendimentos as $atendimento)
                                                    <tr>
                                                        <td class="open_page_paciente" data-id_atd="{{$atendimento->id_atd}}">{{$atendimento->id_atd}}</td>
                                                        <td class="open_page_paciente" data-id_atd="{{$atendimento->id_atd}}">{{$atendimento->nome_pac}}</td>
                                                        <td class="open_page_paciente" data-id_atd="{{$atendimento->id_atd}}">{{$atendimento->email}}</td>
                                                        <td>
                                                            @foreach((array)json_decode($atendimento->testes) as $teste)
                                                                @if($teste->res == '0')
                                                                    <span class="badge badge-outline-danger badge-pill">{{$teste->nome}}</span>
                                                                @elseif($teste->res == '1')
                                                                    <span class="badge badge-outline-success badge-pill">{{$teste->nome}}</span>
                                                                @else
                                                                    <span class="badge badge-outline-warning badge-pill">{{$teste->nome}}</span>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" class="btn-open-modal btn btn-success" data-toggle="modal"  data-id_atd="{{$atendimento->id_atd}}" data-id="{{$atendimento->id_pac}}">Solicitar Teste</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>                                          
                                    </div> <!-- end preview-->
                                </div> <!-- end tab-content-->
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div><!-- end row-->
            </div>
            <!-- container -->
        </div>
        <!-- content -->
    </div>
@endsection
<!--End Body section-->

<!--JS section-->
@section('js')
    <script src="{{asset('atendimento/js/listar.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('socio/assets/js/vendor/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('socio/assets/js/pages/demo.datatable-init.js')}}"></script>
    <script>
        $('.open_page_paciente').click(function(){
            var id = $(this).data("id_atd")
            var url = "paciente/page/"
            base_url + url,
            window.location.replace(base_url + url + id);
        })

        formataData = function(data){
            var partes = data.split("/");
            return partes[2] + "-" + partes[1] + "-" + partes[0]
        }
        
        $('.btn-pesquisar-data').click(function(){

            var data = formataData($('#dash-daterange').val())
            window.location.href = "{{ route('atendimento.listar','')}}" + "/" + data
        })



    </script>
@endsection
<!--End JS section-->


