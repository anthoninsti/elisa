<link href="{{asset('socio/assets/global.css')}}" rel="stylesheet" type="text/css"/>

<div class="modal-header">
    <h4 class="modal-title" id="standard-modalLabel">Solicitar Teste</h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>
<div class="modal-body">
    <h6 id="atd" data-id_atd="{{$id_atd}}">{{$paciente->nome}}</h6>
    <p>{{$paciente->email}}</p>
    <hr>
    <h6>Informe os testes que seram realizados</h6>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane show active" id="input-types-preview">
                            <div class="row">
                                <div class="col-lg-12">
                                <div class="tab-pane show active" id="checkbox-preview">
                                    <h6 class="font-15">Sífilis Recente.</h6>
                                    <div class="mt-3">
                                        @foreach($testes1 as $teste)
                                            <div class="custom-control custom-checkbox line">
                                                <input id="{{$teste->id.'teste'}}" data-id="{{$teste->id}}" type="checkbox" class="custom-control-input" {{$teste->tem_atd}}>
                                                <label class="custom-control-label pcbx" for="{{$teste->id.'teste'}}" style="padding-left: 23px">{{$teste->nome}}</label>
                                                <br><span class="help-block"><small>{{$teste->descicao}}</small></span><br>    
                                            </div>
                                        @endforeach
                                    </div>

                                    <h6 class="font-15 mt-3">Sífilis Tardia</h6>

                                    <div class="mt-3">
                                        @foreach($testes2 as $teste)
                                            <div class="custom-control custom-checkbox line">
                                                <input id="{{$teste->id.'teste'}}" data-id="{{$teste->id}}" type="checkbox" class="custom-control-input" {{$teste->tem_atd}}>
                                                <label class="custom-control-label pcbx" for="{{$teste->id.'teste'}}" style="padding-left: 23px">{{$teste->nome}}</label>
                                                <br><span class="help-block"><small>{{$teste->descicao}}</small></span><br>    
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="example-textarea">Observação</label>
                                        <textarea id="obs" class="form-control" id="example-textarea" rows="5">{{$obs}}</textarea>
                                    </div>
                                </div> 
                            </div>
                            <!-- end row-->                      
                        </div> <!-- end preview-->
                    </div> <!-- end tab-content-->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Fechar</button>
    <button id="btn-save" type="button" class="btn btn-primary">Salvar</button>
</div>  
<style>
.pcbx{
    padding-left: 23px;
}
</style>
<script>
    var config = {
        routes: {
            store: "{{ route('atendimento.store.teste') }}",
        }
    };
</script>
<script src="{{asset('atendimento/js/create_teste.js')}}"></script>