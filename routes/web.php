<?php

Auth::routes();

/*===============================Paciente================================================= */
Route::get('paciente/show/{id}', 'PacienteController@show')->name('paciente.show');
Route::get('paciente/listar', 'PacienteController@index')->name('paciente.listar');
Route::get('paciente/create', 'PacienteController@create')->name('paciente.create');
Route::get('paciente/page/{id}', 'PacienteController@page')->name('paciente.page');
Route::get('paciente/edit/{id}', 'PacienteController@edit')->name('paciente.edit');
Route::post('paciente/update', 'PacienteController@update')->name('paciente.update');
Route::post('paciente/destroy/{id_pac}', 'PacienteController@destroy')->name('paciente.destroy');
Route::get('paciente/alter/teste/{idp}/{idat}', 'PacienteController@loadModalTeste')->name('paciente.alter.teste');

Route::post('paciente/store', 'PacienteController@store')->name('paciente.store');
Route::post('paciente/find', 'PacienteController@find')->name('paciente.find');

/*===============================Cargo================================================= */
Route::get('cargo/listar', 'CargoController@index')->name('cargo.listar');
Route::get('cargo/create', 'CargoController@create')->name('cargo.create');
Route::post('cargo/store', 'CargoController@store')->name('cargo.store');
Route::post('cargo/alter/status', 'CargoController@alterStatus')->name('cargo.alter.status');

/*===============================Funcionario=============================================== */
Route::get('funcionario/login', 'FuncionarioController@login')->name('funcionario.login');
Route::post('funcionario/login', 'Auth\LoginController@login')->name('funcionario.login.store');

Route::get('funcionario/show/{id}', 'FuncionarioController@show')->name('funcionario.show');
Route::get('funcionario/listar', 'FuncionarioController@index')->name('funcionario.listar');
Route::get('funcionario/create', 'FuncionarioController@create')->name('funcionario.create');
Route::post('funcionario/store', 'FuncionarioController@store')->name('funcionario.store');
Route::get('funcionario/edit/{id}', 'FuncionarioController@edit')->name('funcionario.edit');
Route::post('funcionario/update/{id}', 'FuncionarioController@update')->name('funcionario.update');
Route::post('funcionario/destroy', 'FuncionarioController@destroy')->name('funcionario.destroy');

/*===============================Atendimentos=============================================== */
Route::get('Atendimento/listar/{date?}', 'AtendimentoController@index')->name('atendimento.listar');
Route::get('atd/create/teste/{id}/{id_atd}','AtendimentoController@loadModal')->name('atendimento.create.teste');;
Route::post('atd/store/teste','AtendimentoController@storeTestesAtd')->name('atendimento.store.teste');

/*===============================Testes=============================================== */
Route::post('alter','TestController@alter')->name('teste.alter');
Route::post('alter/result','TestController@alterResult')->name('teste.alter.result');


/*===============================Classificação=============================================== */
Route::post('create/destroy','ClassificaController@createOrdestroy')->name('classifica.create_or_destroy');

/*===============================Tratamento=============================================== */
Route::post('find/one','TestTratamentoController@findOne')->name('testtratamento.find_one');


Route::middleware(['auth'])->group(function(){

});


